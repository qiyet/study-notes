<?php
/***
 * 名称：全局应用程序启动文件
 * Alan, 2012.03
 * http://www.fcontex.com/
*/

//程序错误开关	
ini_set('display_errors', 1);

//输出过滤开关
ini_set('magic_quotes_runtime', 0);

//全局属性
define('SYSTEM_INCLUDE', 'YES');
define('SYSTEM_NAME', 'fcontex');
define('SYSTEM_VERSION', '1.0.4');
define('SYSTEM_MODULE', 'SYSTEM|MEMBER|CONTENT|PLUGIN');
define('ERROR_NO_RIGHTS', '没有操作权限。');

//信息类型
define('CONTENT_PAGE', 0);
define('CONTENT_MISC', 1);
define('CONTENT_TEXT', 2);
define('CONTENT_PHOTO', 3);
define('CONTENT_MUSIC', 4);
define('CONTENT_VIDEO', 5);

//物理根目录
define('PATH_ROOT', dirname(__FILE__).'/../');
//读写存储区
define('DIR_STORE', 'store');
define('PATH_STORE', PATH_ROOT.DIR_STORE.'/');
//配置存储区
define('DIR_CONFIG', 'config');
define('PATH_CONFIG', PATH_STORE.DIR_CONFIG.'/');
//缓存目录
define('DIR_CACHE', 'cache');
define('PATH_CACHE', PATH_STORE.DIR_CACHE.'/');
//模块目录
define('DIR_MODULES', 'module');
define('PATH_MODULES', PATH_ROOT.DIR_MODULES.'/');
//插件目录
define('DIR_PLUGINS', 'plugins');
define('PATH_PLUGINS', PATH_ROOT.DIR_PLUGINS.'/');
//主题目录
define('DIR_THEMES', 'themes');
define('PATH_THEMES', PATH_ROOT.DIR_THEMES.'/');
//脚本目录
define('DIR_SCRIPTS', 'scripts');
define('PATH_SCRIPTS', PATH_MODULES.'system/'.DIR_SCRIPTS);
//工具目录
define('DIR_TOOLS', 'tools');
define('PATH_TOOLS', PATH_MODULES.'system/'.DIR_TOOLS);

//字符集
define('CHARSET', 'utf-8');
//session_save_path(PATH_CACHE.'session/');
session_start();
header('Cache-Control: private');
header('Content-Type: text/html;charset='.CHARSET);
date_default_timezone_set('PRC');

include PATH_MODULES.'system/library/cls.application.php';

//全局公共对象
$A = FCApplication::sharedApplication();
$U = FCApplication::sharedMember();
$D = FCApplication::sharedDataBase();
$T = FCApplication::sharedTemplate();
$R = FCApplication::sharedRouter();
$P = FCApplication::sharedPlugin();
$C = FCApplication::sharedContent();

//网站根目录
define('DIR_SITE', $A->getRootDirectory());
//当前模块
define('DIR_MODULE', $R->module);
define('PATH_MODULE', PATH_ROOT.DIR_MODULES.'/'.DIR_MODULE.'/');
//当前主题
define('DIR_THEME', $A->site['site_theme']);
define('PATH_THEME', PATH_ROOT.DIR_THEMES.'/'.DIR_THEME.'/');

//网站根URL
define('URL_SITE', DIR_SITE);
//皮肤URL
define('URL_SKIN', $U->getSkin());
//模块根URL
define('URL_MODULES', URL_SITE.DIR_MODULES.'/');
//当前模块URL
define('URL_MODULE', URL_SITE.DIR_MODULES.'/'.DIR_MODULE.'/');
//主题根URL
define('URL_THEMES', URL_SITE.DIR_THEMES);
//当前主题URL
define('URL_THEME', URL_SITE.DIR_THEMES.'/'.DIR_THEME.'/');
//脚本URL
define('URL_SCRIPTS', URL_MODULES.'system/'.DIR_SCRIPTS.'/');
//工具URL
define('URL_TOOLS', URL_MODULES.'system/'.DIR_TOOLS.'/');
?>