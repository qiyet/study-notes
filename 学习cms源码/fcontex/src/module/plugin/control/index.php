<?php
/***
 * 名称：插件模块动作处理程序
 * Alan 2013.05
 * www.fcontex.com
*/

include '../../startup.php';

switch ($mode = $A->strGet('mode'))
{
	////前端请求////
	
	////后端请求////
	case 'fc.plugin.insert':
	case 'fc.plugin.update':
		$U->checkRights('plugin.insert', TRUE);
		$U->checkRights('plugin.update', TRUE);
		
		$args = $A->strGet('args');
		$plugin = $P->loadPlugin($args);
		if (!$plugin)
		{
			echo '参数错误 [ '.$args.' ] 。';
		}
		else
		{
			//缓存插件组
			$plugins = $A->loadConfig('plugin.plugins');
			$plugins[$args] = $plugin->basic;
			$plugins[$args]['menus'] = $menus = $plugin->getMenus();
			$A->saveConfig('plugin.plugins', $plugins);
			
			//追加插件菜单
			$modules = $A->loadConfig('system.modules');
			if (isset($modules['plugin']))
			{
				$array = array_diff_key($modules['plugin']['menus'], $menus);
				$modules['plugin']['menus'] = array_merge($array, $menus);
			}
			$A->saveConfig('system.modules', $modules);
			
			//更新HOOK列表
			$P->hooksUpdate($args);
			
			//执行安装函数
			if ($mode == 'plugin.insert') $plugin->install();
			$A->logInsert(($mode=='plugin.insert' ? '安装' : '更新') . '了插件 #'.$args);
			echo 'YES';
		}
		break;
		
	case 'fc.plugin.disable':
	case 'fc.plugin.delete':
		$U->checkRights('system.plugins.disable', TRUE);
		$U->checkRights('system.plugins.delete', TRUE);
		
		$args = $A->strGet('args');
		$plugin = $P->loadPlugin($args);
		if (!$plugin)
		{
			echo '参数错误 [ '.$args.' ] 。';
		}
		else
		{
			//清理插件组缓存
			$plugins = $A->loadConfig('plugin.plugins');
			unset($plugins[$args]);
			$plugin->uninstall();
			$A->saveConfig('plugin.plugins', $plugins);
			
			//移除插件菜单
			$modules = $A->loadConfig('system.modules');
			if (isset($modules['plugin']))
			{
				$modules['plugin']['menus'] = array_diff_key($modules['plugin']['menus'], $plugin->getMenus());
			}
			$A->saveConfig('system.modules', $modules);
			
			//更新HOOK列表
			$P->hooksDelete($args);
			
			//执行卸载函数
			$plugin->uninstall();
			$A->logInsert(($mode=='plugin.disable' ? '禁用' : '删除') . '了插件 #'.$args);
			echo 'YES';
		}
		break;
		
	default:
		include PATH_PLUGINS.$A->strGet('plugin').'/control/index.php';
		break;
}
?>