<?php
/*自动化配置文件*/
return array (
  'site_theme' => 'fcontex',
  'site_url' => 'http://www.jrmoc.com/',
  'site_title' => '演示站-基于fcontex主题定制实现企业网站',
  'site_keywords' => 'PHP博客系统,CMS建站系统,WebOS,FreeContex,EasyIDE',
  'site_description' => 'fcontex是基于PHP和SQLite的轻量级内容管理系统(CMS)，同时支持MySQL数据库，用于快速构建中小型网站和个人博客等。是ASP函数库EasyIDE作者alan发起的另一开源项目。',
  'site_email' => '',
  'site_copyright' => '©Copyright 2013',
  'site_emailserver' => '',
  'site_emailpassword' => '',
  'site_rewrite' => false,
  'site_cachetime' => 10,
  'site_name' => 'fcontex',
  'site_pagesize' => 10,
  'site_counter' => '<script type="text/javascript">var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cscript src=\'" + _bdhmProtocol + "hm.baidu.com/h.js%3F974f0214d422da6a1861beb920f423e1\' type=\'text/javascript\'%3E%3C/script%3E"));</script>',
  'site_domain' => 'www.fcontex.com',
  'site_domainlock' => false,
  'site_quiet' => false,
  'site_commentlock' => false,
  'site_gbooklock' => false,
);
?>