程序简介
========
具体效果图见：doc/效果图
fcontex v1.04是基于PHP5.2和MySQL的CMS程序，同时支持SQLite数据库。

授权协议
========

fcontex基于CC协议开源发布，详见license.txt文件。


本地安装
========

将src目录下的所有文件拷贝到IIS或Apache等WEB服务器的虚拟目录下，直接通过浏览器访问：
http://localhost/install
按界面提示开始安装。


远程安装
========

将src目录下的所有文件上传到主机空间中，直接通过浏览器访问：
http://你的域名/install
按界面提示开始安装。


如何制作主题
============

参见doc/1.主题.txt文件


如何开发插件
============

参见doc/2.插件.txt文件


其它问题
========

请到官方网站http://www.fcontex.com/或官方QQ群608812827寻求帮助。
技术支持网站：qiyet.cn
