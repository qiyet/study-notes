/*
Navicat SQLite Data Transfer

Source Server         : desk
Source Server Version : 20817
Source Host           : :0

Target Server Type    : SQLite
Target Server Version : 20817
File Encoding         : 65001

Date: 2013-12-24 16:28:53
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for "main"."fc_angel"
-- ----------------------------
DROP TABLE "main"."fc_angel";
CREATE TABLE "fc_angel" (
"hello"  TEXT
);

-- ----------------------------
-- Records of fc_angel
-- ----------------------------
INSERT INTO "fc_angel" VALUES ('<?php world~ ?>');

-- ----------------------------
-- Table structure for "main"."fc_attached"
-- ----------------------------
DROP TABLE "main"."fc_attached";
CREATE TABLE "fc_attached" (
"at_id"  INTEGER,
"at_uid"  INTEGER DEFAULT 0,
"at_hits"  INTEGER DEFAULT 0,
"at_filename"  TEXT,
"at_filenewname"  TEXT,
"at_title"  TEXT,
"at_isimage"  INTEGER DEFAULT 0,
"at_size"  INTEGER DEFAULT 0,
"at_suffix"  TEXT,
"at_filetype"  TEXT,
"at_dir"  TEXT,
"at_remote"  INTEGER DEFAULT 0,
"at_url"  TEXT,
"at_time"  INTEGER DEFAULT 0,
PRIMARY KEY ("at_id" ASC)
);

-- ----------------------------
-- Records of fc_attached
-- ----------------------------
INSERT INTO "fc_attached" VALUES (1, 1, 0, 'png-1086.png', '735951354083439.png', '', 1, 9791, 'png', 'application/octet-stream', 'member/201211/28/', 0, '', 1354083439);

-- ----------------------------
-- Table structure for "main"."fc_category"
-- ----------------------------
DROP TABLE "main"."fc_category";
CREATE TABLE "fc_category" (
"cg_id"  INTEGER,
"cg_pid"  INTEGER DEFAULT 0,
"cg_order"  INTEGER DEFAULT 0,
"cg_type"  INTEGER DEFAULT 0,
"cg_url"  TEXT,
"cg_target"  TEXT DEFAULT '_self',
"cg_title"  TEXT,
"cg_desc"  TEXT,
"cg_show"  INTEGER DEFAULT 0,
"cg_count"  INTEGER DEFAULT 0,
"cg_time"  INTEGER DEFAULT 0,
PRIMARY KEY ("cg_id" ASC)
);

-- ----------------------------
-- Records of fc_category
-- ----------------------------

-- ----------------------------
-- Table structure for "main"."fc_comment"
-- ----------------------------
DROP TABLE "main"."fc_comment";
CREATE TABLE "fc_comment" (
"cm_id"  INTEGER,
"cm_cid"  INTEGER DEFAULT 0,
"cm_ctitle"  TEXT,
"cm_toid"  INTEGER DEFAULT 0,
"cm_topid"  INTEGER DEFAULT 0,
"cm_uid"  INTEGER DEFAULT 0,
"cm_name"  TEXT,
"cm_toname"  TEXT,
"cm_email"  TEXT,
"cm_url"  TEXT,
"cm_content"  TEXT,
"cm_ip"  TEXT,
"cm_iparea"  TEXT,
"cm_check"  INTEGER DEFAULT 0,
"cm_time"  INTEGER DEFAULT 0,
"cm_update"  INTEGER DEFAULT 0,
"cm_postid"  TEXT,
PRIMARY KEY ("cm_id" ASC)
);

-- ----------------------------
-- Records of fc_comment
-- ----------------------------

-- ----------------------------
-- Table structure for "main"."fc_content"
-- ----------------------------
DROP TABLE "main"."fc_content";
CREATE TABLE "fc_content" (
"ct_id"  INTEGER,
"ct_type"  INTEGER DEFAULT 0,
"ct_cid"  INTEGER DEFAULT 0,
"ct_ctitle"  TEXT,
"ct_uid"  INTEGER DEFAULT 0,
"ct_username"  TEXT,
"ct_title"  TEXT,
"ct_title2"  TEXT,
"ct_hits"  INTEGER DEFAULT 0,
"ct_talks"  INTEGER DEFAULT 0,
"ct_tags"  TEXT,
"ct_cover"  TEXT,
"ct_summary"  TEXT,
"ct_content"  TEXT,
"ct_check"  INTEGER DEFAULT 0,
"ct_fixed"  INTEGER DEFAULT 0,
"ct_quiet"  INTEGER DEFAULT 0,
"ct_inserttime"  INTEGER DEFAULT 0,
"ct_updatetime"  INTEGER DEFAULT 0,
"ct_seo"  INTEGER DEFAULT 0,
"ct_pagetitle"  TEXT,
"ct_keywords"  TEXT,
"ct_description"  TEXT,
PRIMARY KEY ("ct_id" ASC)
);

-- ----------------------------
-- Records of fc_content
-- ----------------------------

-- ----------------------------
-- Table structure for "main"."fc_gbook"
-- ----------------------------
DROP TABLE "main"."fc_gbook";
CREATE TABLE "fc_gbook" (
"gb_id"  INTEGER,
"gb_toid"  INTEGER DEFAULT 0,
"gb_topid"  INTEGER DEFAULT 0,
"gb_uid"  INTEGER DEFAULT 0,
"gb_name"  TEXT,
"gb_toname"  TEXT,
"gb_email"  TEXT,
"gb_url"  TEXT,
"gb_content"  TEXT,
"gb_ip"  TEXT,
"gb_iparea"  TEXT,
"gb_check"  INTEGER DEFAULT 0,
"gb_time"  INTEGER DEFAULT 0,
"gb_update"  INTEGER DEFAULT 0,
"gb_postid"  TEXT,
PRIMARY KEY ("gb_id" ASC)
);

-- ----------------------------
-- Records of fc_gbook
-- ----------------------------

-- ----------------------------
-- Table structure for "main"."fc_group"
-- ----------------------------
DROP TABLE "main"."fc_group";
CREATE TABLE "fc_group" (
"gr_id"  INTEGER,
"gr_name"  TEXT,
"gr_desc"  TEXT,
"gr_rights"  TEXT,
"gr_time"  INTEGER DEFAULT 0,
PRIMARY KEY ("gr_id" ASC)
);

-- ----------------------------
-- Records of fc_group
-- ----------------------------
INSERT INTO "fc_group" VALUES (1, '系统管理员', '系统管理员', 'system.login,system.site.select,system.site.update,system.modules.select,system.modules.insert,system.modules.update,system.modules.disable,system.modules.order,system.modules.delete,system.logs.select,system.logs.delete,system.file.select,system.file.delete,system.file.upload,system.cache,system.skins,system.themes,user.group.select,user.group.insert,user.group.update,user.group.delete,user.select,user.insert,user.update,user.delete,content.category.select,content.category.insert,content.category.update,content.category.order,content.category.delete,content.select,content.insert,content.update,content.delete,content.page.select,content.page.insert,content.page.update,content.page.delete,content.tags.select,content.tags.update,content.tags.delete,content.navigate.select,content.navigate.insert,content.navigate.update,content.navigate.delete,gbook.select,gbook.update,gbook.delete,comment.select,comment.update,comment.delete', 1340031851);
INSERT INTO "fc_group" VALUES (2, '超级管理员', '超级管理员', 'user.select', 1340464421);

-- ----------------------------
-- Table structure for "main"."fc_logs"
-- ----------------------------
DROP TABLE "main"."fc_logs";
CREATE TABLE "fc_logs" (
"lg_id"  INTEGER,
"lg_event"  TEXT,
"lg_uid"  INTEGER DEFAULT 0,
"lg_ip"  TEXT,
"lg_iparea"  TEXT,
"lg_time"  INTEGER DEFAULT 0,
PRIMARY KEY ("lg_id" ASC)
);

-- ----------------------------
-- Records of fc_logs
-- ----------------------------

-- ----------------------------
-- Table structure for "main"."fc_navigate"
-- ----------------------------
DROP TABLE "main"."fc_navigate";
CREATE TABLE "fc_navigate" (
"nv_id"  INTEGER,
"nv_title"  TEXT,
"nv_url"  TEXT,
"nv_target"  INTEGER DEFAULT 0,
"nv_order"  INTEGER DEFAULT 0,
"nv_check"  INTEGER DEFAULT 0,
PRIMARY KEY ("nv_id" ASC)
);

-- ----------------------------
-- Records of fc_navigate
-- ----------------------------
INSERT INTO "fc_navigate" VALUES (1, '首页', '/', 0, 1, 1);
INSERT INTO "fc_navigate" VALUES (2, '交流', '/message/gbook.html', 0, 2, 1);

-- ----------------------------
-- Table structure for "main"."fc_tags"
-- ----------------------------
DROP TABLE "main"."fc_tags";
CREATE TABLE "fc_tags" (
"tg_id"  INTEGER,
"tg_title"  TEXT,
"tg_color"  TEXT,
PRIMARY KEY ("tg_id" ASC)
);

-- ----------------------------
-- Records of fc_tags
-- ----------------------------

-- ----------------------------
-- Table structure for "main"."fc_user"
-- ----------------------------
DROP TABLE "main"."fc_user";
CREATE TABLE "fc_user" (
"us_id"  INTEGER,
"us_group"  INTEGER DEFAULT 0,
"us_username"  TEXT,
"us_password"  TEXT,
"us_name"  TEXT,
"us_desc"  TEXT,
"us_email"  TEXT,
"us_phone"  TEXT,
"us_skin"  TEXT,
"us_face"  TEXT,
"us_time"  INTEGER DEFAULT 0,
"us_resetcodetime"  INTEGER DEFAULT 0,
PRIMARY KEY ("us_id" ASC)
);

-- ----------------------------
-- Records of fc_user
-- ----------------------------
INSERT INTO "fc_user" VALUES (1, 1, 'admin', 'bdN4uqwz', 'admin', '', '', '', 'default', 'member/201211/28/735951354083439.png', 1341129209, 0);
