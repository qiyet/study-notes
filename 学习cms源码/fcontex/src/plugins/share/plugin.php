<?php
/***
 * 名称：通用社会化分享插件
 * Alan 2013.05
 * www.fcontex.com
*/

//类命名方式必须是plugin加首字段大写的目录名才会被内核识别
final class FCPluginShare
{
	////////// 基本信息 //////////
	public $basic = array
	(
		'for'     => '1.0.1+',
		'name'    => '通用分享',
		'icon'    => 'plugin.png',
		'desc'    => '内容和页面分享插件，在内容结束后显示分享功能，可以自由设定代码，比如百度分享或者Jiathis分享等。',
		'author'  => 'alan',
		'contact' => 'alan@fcontex.com',
		'version' => '1.0',
		'update'  => '2013.05',
		'support' => 'www.fcontex.com',
	);
	
	////////// 内部数据 //////////
	
	private $config;
	
	private $menus = array
	(
		'通用分享' => array('plugin'=>'share', 'url'=>'share/config', 'icon'=>'share.png')
	);
	
	//路由HOOK
	private $hooksRouter = array
	(	
	);
	
	//锚点HOOK
	private $hooksAnchor = array
	(
		'content_end' => 'onContentEnd',
	);
	
	public function __construct()
	{
		$this->config = FCApplication::loadConfig('plugin.share', '', '', TRUE);
		if (!is_array($this->config))
		{
			$this->config = array
			(
				'code' => '
					<div style="margin-top:20px; height:18px; padding:10px; background:#eee;">
					<div class="jiathis_style"><span class="jiathis_txt">分享到：</span>
					<a class="jiathis_button_tsina"></a>
					<a class="jiathis_button_tqq"></a>
					<a class="jiathis_button_tsohu"></a>
					<a class="jiathis_button_t163"></a>
					<a class="jiathis_button_renren"></a>
					<a class="jiathis_button_kaixin001"></a>
					<a class="jiathis_button_qzone"></a>
					<a class="jiathis_button_xiaoyou"></a>
					<a class="jiathis_button_cqq"></a>
					<a class="jiathis_button_weixin"></a>
					<a class="jiathis_button_miliao"></a>
					<a class="jiathis_button_douban"></a>
					<a class="jiathis_button_copy"></a>
					<a href="http://www.jiathis.com/share?uid=1767744" class="jiathis jiathis_txt jiathis_separator jtico jtico_jiathis" target="_blank"></a>
					<a class="jiathis_counter_style"></a>
					</div>
					<script type="text/javascript" >
					var jiathis_config =
					{
						data_track_clickback:true,
						summary:"{summary}",
						title:"{title}",
						pic:"{picture}",
						hideMore:false
					}
					</script>
					<script type="text/javascript" src="http://v3.jiathis.com/code_mini/jia.js?uid=1767744" charset="utf-8"></script>
				</div>'
			);
			FCApplication::saveConfig('plugin.share', $this->config);
		}
	}
	
	////////// 对外接口 //////////
	
	public function getMenus()
	{
		return $this->menus;
	}
	
	public function getHooksRouter()
	{
		return $this->hooksRouter;
	}
	
	public function getHooksAnchor()
	{
		return $this->hooksAnchor;
	}
	
	//HOOK函数：在内容结束时执行
	public function onContentEnd($args)
	{
		$id = intval($args);
		if (!$id) return;
		
		global $C, $A;
		$page = $C->getContent('*', 'ct_id = '.$id);
		if (empty($page)) return;
		
		$summary = $A->trim($page['ct_summary'], TRUE);
		$replace = array
		(
			'{summary}' => empty($summary) ? $A->strLeft($A->trim($page['ct_content'], TRUE), 50, '...') : $summary,
			'{title}' => $page['ct_title'].' #'.$A->site['site_name'].'#',
			'{picture}' => empty($page['ct_cover']) ? '' : 'http://'.$A->site['site_domain'].URL_SITE.DIR_STORE.'/'.$A->system['uploadDir'].'/'.$page['ct_cover']
		);
		echo str_replace(array_keys($replace), array_values($replace), $this->config['code']); 
	}
	
	//插件回调函数：在安装完后执行
	public function install()
	{
	}
	
	//插件回调函数：在卸载完成后执行
	public function uninstall()
	{
	}
}
?>