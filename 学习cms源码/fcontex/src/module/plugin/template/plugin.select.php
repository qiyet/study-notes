<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>插件管理 - <?php echo SYSTEM_NAME.' '.SYSTEM_VERSION; ?></title>
<meta name="robots" content="nofollow">
<?php if (0) { ?><link rel="stylesheet" type="text/css" href="../../../module/system/skins/default/style.css" /><?php }else{ ?>
<link rel="stylesheet" type="text/css" href="<?php echo URL_SKIN ?>style.css" /><?php } ?>
<script type="text/javascript" src="<?php echo URL_SCRIPTS; ?>lib.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo URL_SCRIPTS; ?>lib.system.js"></script>
<script type="text/javascript" src="<?php echo URL_SCRIPTS; ?>jquery.ui.min.js"></script>
<script type="text/javascript">
function plugin(mode, args)
{
	switch (mode)
	{
		case 'fc.plugin.insert':
			$$.get('<?php echo $ctrlurl = $R->getCtrlUrl(); ?>?mode=' + mode + '&args=' + args, function(){top.naviUpdate(); $$.redirect();});
			break;
		case 'fc.plugin.delete':
		case 'fc.plugin.disable':
			$$.get('<?php echo $ctrlurl; ?>?mode=' + mode + '&args=' + args, function(){top.naviUpdate(); $$.redirect();});
			break;
		case 'fc.plugin.update':
			$$.get('<?php echo $ctrlurl; ?>?mode=' + mode + '&args=' + args, function(){top.naviUpdate();$$.redirect();});
			break;
		default:
			$$.alert({text : '无效参数 [ '+mode+' ]。'});
			break;
	}
	
	return false;
}
</script>
</head>
<body>

<div class="plugins" id="plugins">
	<?php
	$plugins = $A->loadConfig('plugin.plugins');
	foreach ($plugins as $name => $plugin)
	{
		$dir = PATH_PLUGINS.$name;
		$file = $dir.'/plugin.php';
		if (!file_exists($file)) continue;
	?>
	<dl plugin="<?php echo $name; ?>">
		<dt><img src="<?php echo DIR_SITE.DIR_PLUGINS.'/'.$name.'/icons/'.$plugin['icon']; ?>" /></dt>
		<dd>
			<span>
			<a class="rst" href="javascript:void(0);" onclick="return plugin('fc.plugin.update', '<?php echo $name; ?>');">更新</a> &nbsp; 
			<a class="del" href="javascript:void(0);" onclick="return plugin('fc.plugin.disable', '<?php echo $name; ?>');">禁用</a>
			</span>
			<strong><?php echo $plugin['name'].'&nbsp; &nbsp; '.$name.' v'.$plugin['version']; ?></strong> &nbsp; <small>for <?php echo SYSTEM_NAME.' '.$plugin['for']; ?></small>
		</dd>
		<dd><cite>作者：</cite><?php echo $plugin['author']; ?> (<?php echo $plugin['contact']; ?>) &nbsp; <cite>官方：</cite><a href="http://<?php echo $plugin['support']; ?>/" target="_blank"><?php echo $plugin['support']; ?></a> &nbsp; <cite>更新：</cite><?php echo $plugin['update']; ?></dd>
		<dd><?php echo $plugin['desc']; ?></dd>
	</dl>
	<?php
	}
	$handle = opendir(PATH_PLUGINS);
	while (($name = readdir($handle)) !== false)
	{
		$dir = PATH_PLUGINS.$name;
		if ($name == '.' || $name == '..' || !is_dir($dir) || isset($plugins[$name])) continue;
		$plugin = $P->loadPlugin($name);
		if (!$plugin) continue;
	?>
	<dl class="disabled" plugin="<?php echo $name; ?>">
		<dt><img src="<?php echo DIR_SITE.DIR_PLUGINS.'/'.$name.'/icons/'.$plugin->basic['icon']; ?>" /></dt>
		<dd>
			<span><a class="ins" href="javascript:void(0);" onclick="return plugin('fc.plugin.insert', '<?php echo $name; ?>');">启用</a></span>
			<strong><?php echo $plugin->basic['name'].'&nbsp; &nbsp; '.$name.' v'.$plugin->basic['version']; ?></strong> &nbsp; <small>for <?php echo SYSTEM_NAME.' '.$plugin->basic['for']; ?></small>
		</dd>
		<dd><cite>作者：</cite><?php echo $plugin->basic['author']; ?> (<?php echo $plugin->basic['contact']; ?>) &nbsp; <cite>官方：</cite><a href="http://<?php echo $plugin->basic['support']; ?>/" target="_blank"><?php echo $plugin->basic['support']; ?></a> &nbsp; <cite>更新：</cite><?php echo $plugin->basic['update']; ?></dd>
		<dd><?php echo $plugin->basic['desc']; ?></dd>
	</dl>
	<?php
	}
	closedir($handle);
	?>
	<dl class="more"><dd><a href="http://www.fcontex.com/" target="_blank" class="link">访问官方网站获取更多插件</a></dd></dl>
</div>

</body>
</html>
