<?php
/***
 * 名称：多说评论插件
 * Alan 2013.05
 * www.fcontex.com
*/

//类命名方式必须是plugin加首字段大写的目录名才会被内核识别
final class FCPluginDuoshuo
{
	////////// 基本信息 //////////
	public $basic = array
	(
		'for'     => '1.0.1+',
		'name'    => '多说评论',
		'icon'    => 'plugin.png',
		'desc'    => '多说社会化评论插件，支持以QQ号、微博等方式登录后发表、分享、删除评论，有回复时自动邮件通知。可安全替换fcontex自带的留言和评论功能。',
		'author'  => 'alan',
		'contact' => 'alan@fcontex.com',
		'version' => '1.0',
		'update'  => '2013.05',
		'support' => 'www.fcontex.com',
	);
	
	////////// 内部数据 //////////
	
	private $config;
	
	private $menus = array
	(
		'多说评论' => array('plugin'=>'duoshuo', 'url'=>'duoshuo/config', 'icon'=>'duoshuo.png')
	);
	
	//路由HOOK
	private $hooksRouter = array
	(
		'content/read-'      => 'onRead',
		'content/page-'      => 'onPage',
		'message/gbook.html' => 'onGbook'
	);
	
	//锚点HOOK
	private $hooksAnchor = array
	(
	);
	
	public function __construct()
	{
		$this->config = FCApplication::loadConfig('plugin.duoshuo', '', '', TRUE);
		if (!is_array($this->config))
		{
			$this->config = array
			(
				'api' => 'http://api.duoshuo.com/log/list.json',
				'short_name' => '',
				'secret' => '',
				'since_id' => '0',
				'hook_read' => false,
				'hook_page' => false,
				'hook_gbook' => false,
				'code' => '
					<div class="ds-thread" data-thread-key="{id}" data-title="{title}"></div>
					<script type="text/javascript">
					var duoshuoQuery = {short_name:"{short_name}"};
					(function()
					{
						var ds = document.createElement("script");
						ds.type="text/javascript";
						ds.async = true;
						ds.src = "http://static.duoshuo.com/embed.js";
						ds.charset = "UTF-8";
						(document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(ds);
					})();
					</script>'
			);
			FCApplication::saveConfig('plugin.duoshuo', $this->config);
		}
	}
	
	////////// 对外接口 //////////
	
	public function getMenus()
	{
		return $this->menus;
	}
	
	public function getHooksRouter()
	{
		if (!$this->config['hook_read']) unset($this->hooksRouter['content/read-']);
		if (!$this->config['hook_page']) unset($this->hooksRouter['content/page-']);
		if (!$this->config['hook_gbook']) unset($this->hooksRouter['message/gbook.html']);
		return $this->hooksRouter;
	}
	
	public function getHooksAnchor()
	{
		return $this->hooksAnchor;
	}
	
	private function buildHTML($threadkey, $title)
	{
		return str_replace(array('{id}','{title}','{short_name}'), array($threadkey,$title,$this->config['short_name']), $this->config['code']); 
	}
	
	//HOOK函数：在显示文章时执行
	public function onRead($url, &$output)
	{
		$id = intval(preg_replace('/content\/read-(.+).html/i', '$1', $url));
		if (!$id) return;
		
		global $C; $page = $C->getContent('*', 'ct_id = '.$id);
		if (empty($page)) return;
		
		$output = preg_replace('/<!--comment_js-->[\S\s]+?<!--comment_js-->/i', '', $output);
		$duoshuo = $this->buildHTML($page['ct_id'], $page['ct_title']);
		$output = preg_replace('/<!--comment-->[\S\s]+?<!--comment-->/i', $duoshuo, $output);
	}
	
	//HOOK函数：在显示页面时执行
	public function onPage($url, &$output)
	{
		$id = intval(preg_replace('/content\/page-(.+).html/i', '$1', $url));
		if (!$id) return;
		
		global $C; $page = $C->getContent('*', 'ct_id = '.$id);
		if (empty($page)) return;
		
		$output = preg_replace('/<!--comment_js-->[\S\s]+?<!--comment_js-->/i', '', $output);
		$duoshuo = $this->buildHTML($page['ct_id'], $page['ct_title']);
		$output = preg_replace('/<!--comment-->[\S\s]+?<!--comment-->/i', $duoshuo, $output);
	}
	
	//HOOK函数：在显示留言时执行
	public function onGbook($url, &$output)
	{
		$output = preg_replace('/<!--gbook_js-->[\S\s]+?<!--gbook_js-->/i', '', $output);
		$duoshuo = $this->buildHTML(0, '留言');
		$output = preg_replace('/<!--gbook-->[\S\s]+?<!--gbook-->/i', $duoshuo, $output);
	}
	
	//插件回调函数：在安装完后执行
	public function install()
	{
	}
	
	//插件回调函数：在卸载完成后执行
	public function uninstall()
	{
	}
}
?>