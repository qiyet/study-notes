<?php
/***
 * 名称：内容模块
 * Joe 2012.07
 * www.fcontex.com
*/

if (!defined('SYSTEM_INCLUDE')) exit('Access Denied.');

$T->bind('M', $A->loadLibrary('message'));

switch ($this->action)
{
	////前端请求////
	case 'comment':
		$R->cacheopen = TRUE;
		
		$cid = intval($R->getParam(1,1));
		$page = intval($R->getParam(1,2));
		
		if (!($quiet = $A->site['site_commentlock']))
		{
			$rst = $D->fetch($D->query('select ct_quiet from T[content] where ct_id = '.$cid));
			if ($rst && $rst['ct_quiet']) $quiet = TRUE;
		}
		$T->bind('quiet', $quiet);
		
		$pager = FCApplication::sharedPageTurnner();
		$pager->page = $page;
		$pager->style = 'Simple';
		$pager->linker = "#{p}\" onclick=\"comment.fetch('".$R->getPageUrl('message/comment-'.$cid.'-{p}')."');return false;";
		$query = $pager->parse('cm_id', '*', 'T[comment]', 'cm_check = 1 and cm_topid = 0 and cm_cid = '.$cid, 'cm_id desc');
		$T->bind('comments', $D->fetchAll($query));
		$T->bind('pager', $pager);
		$T->bind('turnner', $pager->turnner);
		$T->display('comment');
		break;
		
	case 'gbook':
		$R->cacheopen = TRUE;
		
		$page = intval($R->getParam(1,1));
		
		$T->bind('quiet', $A->site['site_gbooklock']);
		
		$pager = FCApplication::sharedPageTurnner();
		$pager->page = $page;
		$pager->style = 'Simple';
		$pager->linker = $R->getPageUrl('message/gbook-{p}');
		$query = $pager->parse('gb_id', '*', 'T[gbook]', 'gb_check = 1 and gb_topid = 0 order by gb_id desc');
		$T->bind('gbook', $D->fetchAll($query));
		$T->bind('pager', $pager);
		$T->bind('turnner', $pager->turnner);
		$position[0] = array
		(
			'text' => '留言',
			'link' => $R->getPageUrl('message/gbook')
		);
		$T->bind('position', $position);
		$A->site['site_title'] = $position[0]['text'].' _ '.$A->site['site_title'];
		$T->display('gbook');
		break;
	
	default:
		//$R->cacheopen = TRUE;
		$R->header404();
		$T->display('404');
		break;
	
	////后端请求////
	case 'fc.gbook.select':
		$U->checkRights('gbook.select');
		$T->show('gbook.select');
		break;
	
	case 'fc.gbook.update':
		$U->checkRights('gbook.update');
		$T->show('gbook.update');
		break;
	
	case 'fc.comment.select':
		$U->checkRights('comment.select');
		$T->show('comment.select');
		break;
	
	case 'fc.comment.update':
		$U->checkRights('comment.update');
		$T->show('comment.update');
		break;
}
?>