<?php
include '../module/startup.php';

$mode = $A->strGet('m');
if (!empty($mode))
{
	//准备数据
	$info = array
	(
		'db_base' => $A->strPost('db_base'),
		'db_host' => $A->strPost('db_host'),
		'db_user' => $A->strPost('db_user'),
		'db_pass' => $A->strPost('db_pass'),
		'db_pfix' => $A->strPost('db_pfix'),
		'us_name' => $A->strPost('us_name'),
		'us_pass' => $A->strPost('us_pass'),
	);
	$sql = str_replace('fc_', $info['db_pfix'], file_get_contents('mysql.sql'));
	preg_match_all('/CREATE TABLE[^;]+?;/', $sql, $creates);
	$creates = $creates[0];
	preg_match_all('/INSERT INTO[\s\S]+?\);/', $sql, $inserts);
	$inserts = $inserts[0];
	
	//连接数据库
	$db = @database::connect('mysql', $info['db_base'], $info['db_user'], $info['db_pass'], $info['db_host']);
	if (!$db->conn)
	{
		exit('ERR|安装失败：无法完成数据库初始化，请确认连接信息是否填写正确。');
	}
	else
	{
		//执行SQL语句
		$sqls = array_merge($creates, $inserts);
		foreach ($sqls as $sql)
		{
			if(!@$db->query($sql, $db->conn))
			{
				exit('ERR|安装失败：执行语句出错：<br />'.$sql.'<br /><br />请确认：<br />1、数据库连接信息是否正确；<br />2、数据库['.$info['db_base'].']是否已创建。');
			}
		}
		
		//更新配置
		$config = $A->loadConfig('system.database');
		$config['DB_TYPE'] = 'mysql';
		$config['DB_BASE'] = $info['db_base'];
		$config['DB_HOST'] = $info['db_host'];
		$config['DB_USER'] = $info['db_user'];
		$config['DB_PASS'] = $info['db_pass'];
		$config['DB_CHAR'] = 'utf8';
		$config['DB_HOLD'] = TRUE;
		$config['DB_PFIX'] = $info['db_pfix'];
		$A->saveConfig('system.database', $config);
		
		//更新管理员
		$sql = 'update '.$info['db_pfix'].'user set us_username = "'.$info['us_name'].'", us_password="'.$A->strEnCode($info['us_pass']).'" where us_id = 1';
		$db->query($sql, $db->conn);
		
		//清理文件
		@$A->cleanDirectory('../install', TRUE);
		echo('YES|安装成功，<span style="color:#f00;">请检查并确认[install]目录已被删除。</span>。');
	}
}
else
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo SYSTEM_NAME.' '.SYSTEM_VERSION; ?> 安装程序</title>
<link rel="stylesheet" type="text/css" href="images/style.css" />
<script type="text/javascript" src="images/jquery.js"></script>
<script type="text/javascript" src="images/jquery.fullscreenr.js"></script>
<script type="text/javascript">
function backResize()
{
	var backIMG = $('#backimg');
	$.fn.fullscreenr({width:backIMG.width(), height:backIMG.height(), bgID:'#backimg'});
	backIMG.fadeIn(1000);
}
function install()
{
	$('#tips_2').html('');
		
	if ($.trim($('#db_base').val()) == '')
	{
		$('#tips_2').html('错误：请输入数据库名称。');
		$('#db_base').focus();
		return;
	}
	else if ($.trim($('#db_host').val()) == '')
	{
		$('#tips_2').html('错误：请输入数据库地址。');
		$('#db_host').focus();
		return;
	}
	else if ($.trim($('#db_user').val()) == '')
	{
		$('#tips_2').html('错误：请输入数据库用户名。');
		$('#db_user').focus();
		return;
	}
	else if ($.trim($('#db_pass').val()) == '')
	{
		$('#tips_2').html('错误：请输入数据库密码。');
		$('#db_pass').focus();
		return;
	}
	else if ($.trim($('#db_pfix').val()) == '')
	{
		$('#tips_2').html('错误：请输入数据表前缀。');
		$('#db_pfix').focus();
		return;
	}
	else if ($.trim($('#us_name').val()) == '')
	{
		$('#tips_2').html('错误：请输入管理员帐户。');
		$('#us_name').focus();
		return;
	}
	else if ($.trim($('#us_pass').val()) == '')
	{
		$('#tips_2').html('错误：请输入管理员密码。');
		$('#us_pass').focus();
		return;
	}
	else if ($.trim($('#us_pass_2').val()) == '')
	{
		$('#tips_2').html('错误：请确认管理员密码。');
		$('#us_pass_2').focus();
		return;
	}
	else if ($('#us_pass_2').val() != $('#us_pass').val())
	{
		$('#tips_2').html('错误：密码输入不一致。');
		$('#us_pass_2').select();
		return;
	}
		
	$.ajax
	({
		type       : 'post',
		url        : '?m=GO',
		cache      : false,
		data       : $('#form_install').serialize(),
		beforeSend : function(XMLHttpRequest)
		{
			$('#tips_3').html('安装中请稍候...');
			$('#step_2').slideUp(300);
			$('#step_3').slideDown(300);
		},
		success    : function(data, textStatus)
		{
			var a = data ? data.split('|') : ['无效的服务器响应。'];
			if (a[0] == 'YES')
			{
				$('#tips_3').html(a[1]);
				$('#button_3_1').show(100);
			}
			else if (a[0] == 'ERR')
			{
				$('#tips_3').html(a[1]);
				$('#button_3_0').show(100);
			}
			else
			{
				$('#tips_3').html(data);
				$('#button_3_0').show(100);
			}
		},
		error     : function(XMLHttpRequest, textStatus, errorThrown)
		{
			$('#tips_3').html('超求超时。');
			$('#button_3_0').show(100);
		}
	});
}
$(function()
{
	window.setTimeout(backResize, 500);
	
	$('#button_1').click(function()
	{
		$('#step_1').slideUp(300);
		$('#step_2').slideDown(300);
	});
	
	$('#button_3_0').click(function()
	{
		$('#tips_2').html('');
		$('#button_3_0').hide();
		$('#step_3').slideUp(300);
		$('#step_2').slideDown(300);
	});
});
</script>
</head>

<body>
	<div class="backimgMask"></div>
	<img id="backimg" src="images/back.jpg" />
	<div class="toper w_700 filter_8">
		<h1>欢迎进入 <?php echo SYSTEM_NAME.' '.SYSTEM_VERSION; ?> 安装程序</h1>
		<p>- We create awesome stuff -</p> 
	</div>
	<div class="content w_700 filter_8">
	
		<div id="step_1" class="steps" style="display:block;">
			<h2>阅读发行协议</h2>
			fcontex采用知识共享署名-非商业性使用-相同方式共享 2.5 中国大陆许可协议进行许可<br />
			协议概要请见：<br />
			<a href="http://creativecommons.org/licenses/by-nc-sa/2.5/cn/" target="_blank">http://creativecommons.org/licenses/by-nc-sa/2.5/cn/</a><br /><br />
			<strong>Creative Commons (CC)<br />
			署名-非商业性使用-相同方式共享 2.5 中国大陆 (CC BY-CN-SA 2.5)<br /></strong>
			您可以自由：<br />
				复制、发行、展览、表演、放映、广播或通过信息网络传播本作品；创作演绎作品；<br />
			惟须遵守下列条件：<br />
				署名 — 您必须按照作者或者许可人指定的方式对作品进行署名。<br />
				非商业性使用 — 您不得将本作品用于商业目的。<br />
				相同方式共享 — 如果您改变、转换本作品或者以本作品为基础进行创作，您只能采用与本协议相同的许可协议发布基于本作品的演绎作品。<br />
		 	Notice — 对任何再使用或者发行，您都必须向他人清楚地展示本作品使用的许可协议条款。 <br /><br />
			法律文本详见：<br />
			<a href="http://creativecommons.org/licenses/by-nc-sa/2.5/cn/legalcode" target="_blank">http://creativecommons.org/licenses/by-nc-sa/2.5/cn/legalcode</a>
			<div class="button"><a class="button" id="button_1">我已阅读并承诺遵守此协议</a></div>
			<div class="clear"></div>
		</div>
		
		<div id="step_2" class="steps">
			<form id="form_install" method="post" action="?" onsubmit="install('s');return false;">
				<h2>MySQL数据库信息</h2>
				<dl>
					<dt>数据库名称：</dt>
					<dd style="height:80px;"><input type="text" class="text" name="db_base" id="db_base" /><br /><span id="db_base_tips">请先手动创建该名称的MySQL数据库，创建时：<br />[字 符 集] 选择“utf8 -- UTF-8 Unicode”，<br />[排序规格] 选择“utf8_general_ci”。</span></dd>
					<dt>数据库地址：</dt>
					<dd><input type="text" class="text" name="db_host" id="db_host" value="127.0.0.1" /></dd>
					<dt>数据库用户名：</dt>
					<dd><input type="text" class="text" name="db_user" id="db_user" value="root" /></dd>
					<dt>数据库密码：</dt>
					<dd><input type="text" class="text" name="db_pass" id="db_pass" /></dd>
					<dt>数据库表前辍：</dt>
					<dd><input type="text" class="text" name="db_pfix" id="db_pfix" value="fc_" /></dd>
				</dl>
				<div class="clear"></div>
				<h2>网站管理员信息</h2>
				<dl>
					<dt>管理员账号：</dt>
					<dd><input type="text" class="text" name="us_name" id="us_name" value="admin" /></dd>
					<dt>管理员密码：</dt>
					<dd><input type="password" class="text" name="us_pass" id="us_pass" /></dd>
					<dt>确认密码：</dt>
					<dd><input type="password" class="text" id="us_pass_2" /></dd>
				</dl>
				<div class="clear"></div>
				<div class="button"><a class="button" id="button_2" onclick="install('s');">确认安装</a></div>
				<div class="clear"></div>
				<div style="display:none;"><input type="submit" /></div>
				<div id="tips_2"></div>
			</form>
		</div>
		
		<div id="step_3" class="steps">
			<div id="tips_3"></div>
			<div class="button">
				<a class="button" id="button_3_0" style="display:none;">返回</a>
				<a class="button" id="button_3_1" onclick="location.href='../?system.html';" style="display:none;">完成</a>
			</div>
			<div class="clear"></div>
		</div>
		
	</div>

</body>
</html>
<?php
}
?>