<?php
/***
 * 名称：插件模块类
 * Alan 2013.05
 * www.fcontex.com
*/

//类命名方式必须是FCModule加首字段大写的目录名才会被内核识别
final class FCModulePlugin
{
	/***
	 * 模块基本信息
	*/
	public $basic = array
	(
		'for'     => '1.0.1+',
		'name'    => '插件管理',
		'icon'    => 'module.png',
		'desc'    => '插件模块，提供插件的管理功能。',
		'author'  => 'alan',
		'contact' => 'alan@fcontex.com',
		'version' => '1.0',
		'update'  => '2013.05',
		'support' => 'www.fcontex.com',
	);
	
	/***
	 * 模块权限字段 安装时注册到内核中
	 * 为保证全局唯一请使用模块目录名加下划线作前缀
	*/
	public $rights = array
	(
		'plugin.select'  => '插件查看',
		'plugin.insert'  => '插件安装',
		'plugin.update'  => '插件更新',
		'plugin.disable' => '插件禁用',
		'plugin.order'   => '插件排序',
		'plugin.delete'  => '插件卸载',
	);
	
	/***
	 * 模块功能菜单
	*/
	public $menus = array
	(
		'插件管理' => array('url'=>'fc.plugin.select', 'icon'=>'plugin.select.png', 'rights'=>'plugin.select')
	);
	
	/***
	 * 模块安装回调函数
	 * 用于完成模块安装时的初始化工作
	*/
	public function install()
	{
		global $A;
		
		$modules = $A->loadConfig('system.modules', '', '', TRUE);
		$menus = $modules['plugin']['menus'];
		foreach ($A->loadConfig('plugin.plugins') as $plugin)
		{
			$menus = array_merge($menus, $plugin['menus']);
		}
		$modules['plugin']['menus'] = $menus;
		$A->saveConfig('system.modules', $modules);
	}
	
	/***
	 * 模块反安装回调函数
	 * 用于完成模块卸载时的清理工作
	*/
	public function uninstall()
	{
	}
}
?>