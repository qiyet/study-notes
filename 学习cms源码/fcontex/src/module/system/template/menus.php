<ul>
	<?php
	//主菜单
	$iconDefault = URL_SKIN.'module.default.png';
	foreach ($A->loadconfig('system.modules') as $name => $module)
	{
		ob_start();
		$n = 0;
	?>
	<li module="<?php echo $name; ?>" title="<?php echo $module['name']; ?>" onmouseover="$(this).addClass('hover');" onmouseout="$(this).removeClass('hover');" onclick="naviClick(this);">
		<img src="<?php echo isset($module['icon']) ? DIR_SITE.DIR_MODULES.'/'.$name.'/icons/'.$module['icon'] : $iconDefault; ?>" />
		<dl>
			<?php
			//子菜单
			foreach ($module['menus'] as $text => $value)
			{
				if (isset($value['rights']) && !$U->hasRights($value['rights'])) continue;
				if (!empty($value['icon']))
				{
					$icon = DIR_SITE.(isset($value['plugin']) ? DIR_PLUGINS.'/'.$value['plugin'] : DIR_MODULES.'/'.$name).'/icons/'.$value['icon'];
				}
				else
				{
					$icon = $iconDefault;
				}
			?>
			<dd navigate="<?php echo $name.'/'.$value['url']; ?>" onmouseover="$(this).addClass('hover');" onmouseout="$(this).removeClass('hover');" onclick="menuClick(this);">
				<img src="<?php echo $icon; ?>" />
				<div class="icon"><img src="<?php echo $icon; ?>" /></div>
				<div class="text"><span class="L"><?php echo $text; ?></span><span class="R"></span><span class="T"><?php echo $R->getPageUrl($name.'/'.$value['url']); ?></span></div>
			</dd>
			<?php
				$n++;
			}
			?>
		</dl>
	</li>
	<?php
		$n==0 ? ob_end_clean() : ob_end_flush();
	}
	?>
</ul>