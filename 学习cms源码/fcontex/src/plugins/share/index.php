<?php
/***
 * 名称：插件内部调度程序
 * Alan 2013.05
 * www.fcontex.com
*/

if (!defined('SYSTEM_INCLUDE')) exit('Access Denied.');

$T->root = PATH_PLUGINS.$R->plugin.'/template/';
switch ($R->action)
{
	case 'config':
		$T->show('config');
		break;
	
	default:
		//$R->cacheopen = TRUE;
		$R->header404();
		$T->display('404');
		break;
}
?>