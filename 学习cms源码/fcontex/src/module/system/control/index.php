<?php
/***
 * 名称：系统模块动作处理程序
 * Alan 2012.03
 * www.fcontex.com
*/

include '../../startup.php';

switch ($mode = $A->strGet('mode'))
{
	////前端请求////
	
	////后端请求////
	case 'fc.module.insert':
	case 'fc.module.update':
		$U->checkRights('system.modules.insert', TRUE);
		$U->checkRights('system.modules.update', TRUE);
		
		$args = $A->strGet('args');
		$path = PATH_MODULES.$args.'/module.php';
		include $path;
		$class = 'FCModule'.ucfirst($args);
		if (!class_exists($class))
		{
			echo '参数错误 [ '.$args.' ] 。';
		}
		else
		{
			//缓存模块组
			$module = new $class();
			$modules = $A->loadConfig('system.modules');
			$modules[$args] = $module->basic;
			$modules[$args]['menus'] = $module->menus;
			$modules[$args]['rights'] = $module->rights;
			$A->saveConfig('system.modules', $modules);
			
			//执行安装函数
			$module->install();
			$A->logInsert(($mode=='module.insert' ? '安装' : '更新') . '了模块 #'.$args);
			$P->hookAnchor(str_replace('.', '_', $mode), $args);
			echo 'YES';
		}
		break;
		
	case 'fc.module.disable':
	case 'fc.module.delete':
		$U->checkRights('system.modules.disable', TRUE);
		$U->checkRights('system.modules.delete', TRUE);
		
		$args = $A->strGet('args');
		if (in_array(strtoupper($args), explode('|', SYSTEM_MODULE)))
		{
			echo 'ERR|核心模块不允许禁用。';
		}
		else
		{
			$path = PATH_MODULES.$args.'/module.php';
			include $path;
			$class = 'FCModule'.ucfirst($args);
			if (!class_exists($class))
			{
				echo '参数错误 [ '.$args.' ] 。';
			}
			else
			{
				//清理模块组缓存
				$module = new $class();
				$modules = $A->loadConfig('system.modules');
				unset($modules[$args]);
				$module->uninstall();
				$A->saveConfig('system.modules', $modules);
				
				//执行卸载函数
				$module->uninstall();
				$A->logInsert(($mode=='module.disable' ? '禁用' : '删除') . '了模块 #'.$args);
				$P->hookAnchor('module_delete', $args);
				echo 'YES';
			}
		}
		break;
		
	case 'fc.module.order':
		$U->checkRights('system.modules.order', TRUE);
		
		$args = $A->strGet('args');
		$old = $A->loadConfig('system.modules');
		$new = array();
		foreach (explode(',', $args) as $key)
		{
			if (isset($old[$key])) $new[$key] = $old[$key];
		}
		$A->saveConfig('system.modules', $new);
		
		$A->logInsert('更新了模块排序');
		
		echo 'YES';
		
		break;
	
	//站点配置
	case 'fc.config.site':
		$U->checkRights('system.site.update', TRUE);
		
		$data = $A->site;
		
		$data['site_name'] 			= $A->strPost('site_name', FALSE);
		$data['site_domain'] 		= str_replace('http://', '', $A->strPost('site_domain', FALSE));
		$data['site_domainlock']	= intval($A->strPost('site_domainlock')) ? TRUE : FALSE;
		//$data['site_rooturl'] 		= 'http://'.preg_replace(array('/^http:\/\//i', '/\/$/i'), '', $A->strPost('site_rooturl'));
		$data['site_title'] 		= $A->strPost('site_title', FALSE);
		$data['site_keywords'] 		= $A->strPost('site_keywords', FALSE);
		$data['site_description'] 	= $A->strPost('site_description', FALSE);
		$data['site_rewrite'] 		= intval($A->strPost('site_rewrite')) ? TRUE : FALSE;
		$data['site_pagesize'] 		= intval($A->strPost('site_pagesize'));
		$data['site_commentlock']	= intval($A->strPost('site_commentlock')) ? TRUE : FALSE;
		$data['site_gbooklock'] 	= intval($A->strPost('site_gbooklock')) ? TRUE : FALSE;
		$data['site_email'] 		= $A->strPost('site_email', FALSE);
		$data['site_counter'] 		= $A->strPost('site_counter', FALSE);
		$data['site_copyright'] 	= $A->strPost('site_copyright', FALSE);
		$data['site_emailserver'] 	= $A->strPost('site_emailserver', FALSE);
		$site_emailpassword = $A->strPost('site_emailpassword', FALSE);
		$data['site_emailpassword'] = $site_emailpassword==$data['site_emailpassword'] ? $site_emailpassword : $A->strEnCode($site_emailpassword);
		
		$A->saveConfig('system.site', $data);
		$A->logInsert('更新了站点配置');
		$P->hookAnchor('system_site');
		echo 'YES';
		break;
	
	case 'fc.config.skin':
		$U->checkRights('system.skins', TRUE);
		
		$args = trim($A->strGet('args'));
		if ($args == '')
		{
			echo 'ERR|皮肤设置失败。';
		}
		else 
		{
			$D->update('T[user]', array('us_skin' => $args), array('us_id' => $_SESSION['userInfo']['us_id']));
			$_SESSION['userInfo']['us_skin'] = $args;
			$A->logInsert('更换了皮肤 # '.$args);
			$P->hookAnchor('system_skin', $args);
			echo 'YES';
		}
		break;
	
	case 'fc.config.theme':
		$U->checkRights('system.themes', TRUE);
		
		$args = trim($A->strGet('args'));
		if ($args == '')
		{
			echo 'ERR|主题设置失败。';
		}
		else 
		{
			$data = $A->site;
			$data['site_theme'] = $args;
			$A->saveConfig('system.site', $data);
			$A->logInsert('更换了主题 # '.$args);
			$P->hookAnchor('system_theme', $args);
			echo 'YES';
		}
		break;
	
	case 'fc.logs.delete':
		$U->checkRights('system.logs.delete', TRUE);
		
		$args = $A->strGet('args');
		if (trim($args) == '')
		{
			echo 'ERR|请选择记录。';
		}
		else 
		{
			if ($args == '*')
			{
				$D->delete('T[logs]', '1=1');
			}
			else
			{
				$D->delete('T[logs]', 'lg_id in ('.$args.')');
			}
			
			$A->logInsert('清理了日志');
			$P->hookAnchor('logs_delete', $args);
			
			echo 'YES';
		}
		break;
	
	case 'fc.file.update':
		$U->checkRights('system.file.update', TRUE);
		
		$args = $A->strGet('args');
		$at_filename = trim($A->strPost('at_filename'));
		if (!is_numeric($args))
		{
			echo 'ERR|错误的参数[ '.$args.' ]。';
		}
		elseif ($at_filename == '')
		{
			echo 'ERR|请输入文件名。|at_filename_'.$args;
		}
		else
		{
			$updatearr = array
			(
				'at_filename' => $at_filename
			);
			$D->update('T[attached]', $updatearr, array('at_id' => $args));
			$A->logInsert('编辑了附件 #'.$args);
			$P->hookAnchor('file_update', $args);
			echo 'YES';
		}
		break;
	
	case 'fc.file.delete':
		$U->checkRights('system.file.delete', TRUE);
		
		$args = $A->strGet('args');
		if (trim($args) == '')
		{
			echo 'ERR|请选择记录。';
		}
		else 
		{
			$arr = explode(',', $args);
			$uploadPath = $A->system['uploadPath'];
			foreach ($arr as $id)
			{
				if (!intval($id)) continue;
				$res = $D->query('select at_remote, at_dir, at_filenewname, at_id from T[attached] where at_id = '.intval($id));
				$rst = $D->fetch($res);
				if (!$rst['at_remote'])
				{
					$path = $uploadPath.$rst['at_dir'].$rst['at_filenewname'];
					if (file_exists($path)) unlink($path);
				}
				$D->delete('T[attached]', 'at_id = '.$rst['at_id']);
			}
			$A->logInsert('删除了附件 # '.$args);
			$P->hookAnchor('file_delete', $args);
			echo 'YES';
		}
		break;
	
	case 'fc.file.upload':
		//当前采用的FLASH上传控件无法共享会话状态需要传参重建
		$U->session($A->strPost('passport'));
		$U->checkRights('system.file.upload', TRUE);
		
		$fileDir = $A->strPost('dir');
		$config_filetype = $A->system['uploadSuffix'];
		$fileType = $A->strPost('type');
		$suffix = isset($config_filetype[$fileType]) ? $config_filetype[$fileType] : $config_filetype['image'];
		$upload = $A->loadLibrary('fileupload');
		$upload->fileLimitSuffix = $suffix;
		$upload->fileNewDir = $fileDir;
		$data = $upload->upload();
		//上传成功
		if ($data['error'] == '')
		{
			//存入数据库
			$insertarr = array
			(
				'at_filenewname'	=> $data['fileNewName'],
				'at_size'			=> $data['fileSize'],
				'at_suffix'			=> $data['fileSuffix'],
				'at_dir'			=> $data['fileDir'],
				'at_filename'		=> $data['fileName'],
				'at_isimage'		=> $data['isImage'],
				'at_filetype'		=> $data['fileType'],
				'at_uid'			=> $_SESSION['userInfo']['us_id'],
				'at_time'			=> time()
			);
			$D->insert('T[attached]', $insertarr);
			$data['fileID'] = $D->insertid('T[attached]');
			$data['filePipe'] = $R->getPageUrl('system/file/'.$A->strEnCode($data['fileID']), '', DIR_SITE);
			
			$A->logInsert('上传了附件 # '.$data['fileID'], $_SESSION['userInfo']['us_id']);
			$P->hookAnchor('file_insert', $data['fileID']);
		}
		$data['fileUrl'] = DIR_SITE.DIR_STORE.'/'.$A->system['uploadDir'].'/'.$data['fileDir'].$data['fileNewName'];
		$data['fileIcon'] = $A->fileIcon($A->fileSuffix($data['fileName']));
		echo json_encode($data);
		break;
	
	case 'fc.file.remote':
		$U->checkRights('system.file.upload', TRUE);
		
		$image = intval($A->strPost('image'));
		$url = $A->strPost('url');
		$title = $A->strPost('title');
		$insertarr = array
		(
			'at_uid'		=> $_SESSION['userInfo']['us_id'],
			'at_filename'	=> $title,
			'at_isimage'	=> $image,
			'at_remote'		=> 1,
			'at_url'		=> $url,
			'at_title'		=> $title,
			'at_time'		=> time()
		);
		$D->insert('T[attached]', $insertarr);
		$insertid = $D->insertid('T[attached]');
		$A->logInsert('添加了附件 # '.$insertid, $_SESSION['userInfo']['us_id']);
		$P->hookAnchor('file_insert', $insertid);
		$data = array
		(
			'fileID' => $insertid,
			'filePipe' => $R->getPageUrl('system/file/'.$A->strEnCode($insertid), '', DIR_SITE)
		);
		echo 'YES|'.json_encode($data);
		break;
	
	case 'fc.cache.clean':
		$U->checkRights('system.cache', TRUE);
		
		$args = $A->strGet('args');
		foreach (explode(',', $args) as $name)
		{
			$name = PATH_CACHE.$name;
			if (is_dir($name))
			{
				$A->cleanDirectory($name);
			}
			else
			{
				unlink($name);
			}
		}
		$A->logInsert('清理了缓存 # '.$args);
		$P->hookAnchor('cache_clean', $args);
		echo 'YES';
		break;
	
	case 'fc.cache.time':
		$U->checkRights('system.cache', TRUE);
		
		$args = $A->strGet('args');
		$data = $A->site;
		$data['site_cachetime'] = intval($args);
		$A->saveConfig('system.site', $data);
		
		$A->logInsert('更新了缓存时间 #'.$args);
		$P->hookAnchor('cache_time', $args);
		echo 'YES';
		break;
		
	default:
		echo 'ERR|无效请求。';
		break;
}
?>