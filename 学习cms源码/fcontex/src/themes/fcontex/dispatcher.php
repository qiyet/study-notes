<?php
/***
 * 名称：主题请求分程序
 *      可选文件，如果使用了此文件，则所有主题请求由此文件进行分发处理，否则由系统默认方式分发处理。
 * Alan 2013.12
 * www.fcontex.com
*/

if (!defined('SYSTEM_INCLUDE')) exit('Access Denied.');

//在后面添加栏目后在这里配置
$p_pid = '2';    //产品父栏目ID
$p_ids = '5,6';  //产品子栏目ID列表
$n_pid = '3';    //新闻父栏目ID
$n_ids = '7,8';  //新闻子栏目ID列表
$r_pid = '4';    //首页滚动栏目ID

$T->bind('links', $C->getContents('*', 'ct_cid=1 and ct_check=1'));
switch ($R->action)
{
	case 'start':
		$R->cacheopen = TRUE;
		
		//大图滚动
		$sql = 'select ct_cover, ct_title2 from T[content] where ct_cid='.$r_pid.' and ct_check=1 order by ct_inserttime desc limit 6';
		$focuses = $D->query($sql);
		$T->bind('focuses', $D->fetchAll($focuses));
		//小图产品
		$sql  = 'select ct_id, ct_title, ct_title2, ct_cover from T[content] where  ct_check=1 ';
		$sql .= 'and ct_fixed=1 and ct_cid in ('.$p_ids.') order by ct_inserttime desc limit 4';
		$products = $D->query($sql);
		$T->bind('products', $D->fetchAll($products));
		//滚动新闻
		$sql  = 'select ct_id, ct_title from T[content] where ct_check=1 ';
		$sql .= 'and ct_fixed=1 and ct_cid in ('.$n_ids.') order by ct_inserttime desc limit 4';
		$contents = $D->query($sql);
		$T->bind('contents', $D->fetchAll($contents));
		
		$T->display('start');
		break;
	
	case 'product':
		$R->cacheopen = TRUE;
		
		$id = intval($R->getParam(1,0));
		$cid = intval($R->getParam(1,1));
		$page = intval($R->getParam(1,2));
		
		$T->bind('banner', 'page_product.jpg');
		$T->bind('categories', $C->getCategories($p_pid));
		if ($id > 0)
		{
			$page = $C->getContent('*', 'ct_id = '.$id);
			if (empty($page))
			{
				$R->print404();
			}
			$A->site['site_title'] = $page['ct_title'].' _ '.$A->site['site_title'];
			$A->site['site_description'] = $A->trim($page['ct_summary'], TRUE);
			$A->site['site_keywords'] = $page['ct_title'].$A->site['site_keywords'];
			$T->bind('page', $page);
			$T->display('product.read');
		}
		else
		{
			$pager = FCApplication::sharedPageTurnner();
			$pager->style = 'Simple';
			$pager->page = $page;
			$pager->size = $A->site['site_pagesize'];
			$pager->linker = $R->getPageUrl('product/list-'.$id.'-{p}');
			$field = 'ct_id, ct_username, ct_title, ct_tags, ct_summary, ct_content, ct_inserttime, ct_cover, ct_uid, ct_hits';
			$where = 'ct_check=1';
			$order = 'ct_inserttime desc';
			if ($cid > 0)
			{
				$where .= ' and ct_cid = '.$cid;
				$items = $pager->parse('ct_id', $field, 'T[content]', $where, $order);
				$category = $C->getCategory($cid);
			}
			else
			{
				$where .= ' and ct_cid in ('.$p_ids.')';
				$items = $pager->parse('ct_id', $field, 'T[content]', $where, $order);
			}
			
			if (empty($category))
			{
				$category['cg_id'] = 0;
				$category['cg_title'] = '主营产品';
			}
			
			$A->site['site_title'] = $category['cg_title'].' _ '.$A->site['site_title'];
			$A->site['site_description'] = $category['cg_title'].'|'.$A->site['site_description'];
			$A->site['site_keywords'] = $category['cg_title'].','.$A->site['site_keywords'];
			$T->bind('turnner', $pager->turnner);
			$T->bind('items', $D->fetchAll($items));
			$T->bind('category', $category);
			$T->display('product.list');
		}
		break;
	
	case 'news':
		$R->cacheopen = TRUE;
		
		$id = intval($R->getParam(1,0));
		$cid = intval($R->getParam(1,1));
		$page = intval($R->getParam(1,2));
		
		$T->bind('banner', 'page_news.jpg');
		$T->bind('categories', $C->getCategories($n_pid));
		if ($id > 0)
		{
			$page = $C->getContent('*', 'ct_id = '.$id);
			if (empty($page))
			{
				$R->print404();
			}
			$A->site['site_title'] = $page['ct_title'].' _ '.$A->site['site_title'];
			$A->site['site_description'] = $A->trim($page['ct_summary'], TRUE);
			$A->site['site_keywords'] = $page['ct_title'].','.$A->site['site_keywords'];
			$T->bind('page', $page);
			$T->display('news.read');
		}
		else
		{
			$pager = FCApplication::sharedPageTurnner();
			$pager->style = 'Simple';
			$pager->page = $page;
			$pager->size = $A->site['site_pagesize'];
			$pager->linker = $R->getPageUrl('news/list-'.$cid.'-{p}');
			$field = 'ct_id, ct_username, ct_title, ct_tags, ct_summary, ct_content, ct_inserttime, ct_cover, ct_uid, ct_hits';
			$where = 'ct_check=1';
			$order = 'ct_fixed desc, ct_inserttime desc';
			if ($cid > 0)
			{
				$where .= ' and ct_cid = '.$cid;
				$items = $pager->parse('ct_id', $field, 'T[content]', $where, $order);
				$category = $C->getCategory($cid);
			}
			else
			{
				$where .= ' and ct_cid in ('.$n_ids.')';
				$items = $pager->parse('ct_id', $field, 'T[content]', $where, $order);
			}
			
			if (empty($category))
			{
				$category['cg_id'] = 0;
				$category['cg_title'] = '新闻中心';
			}
			
			$A->site['site_title'] = $category['cg_title'].' _ '.$A->site['site_title'];
			$A->site['site_description'] = $A->site['site_description'];
			$A->site['site_keywords'] = $category['cg_title'].','.$A->site['site_keywords'];
			$T->bind('turnner', $pager->turnner);
			$T->bind('items', $D->fetchAll($items));
			$T->bind('category', $category);
			$T->display('news.list');
		}
		break;
	
	case 'search':
		$keyw = urldecode($R->getParam(1));
		$page = intval($R->getParam(2));
		$T->bind('keyw', $keyw);
		
		$pager = FCApplication::sharedPageTurnner();
		$pager->style = 'Simple';
		$pager->page = $page;
		$pager->size = $A->site['site_pagesize'];
		$field = 'ct_id, ct_username, ct_title, ct_tags, ct_summary, ct_content, ct_inserttime, ct_cover, ct_uid, ct_hits';
		$where = 'ct_check=1 and (ct_title like "%'.$keyw.'%" or ct_tags like "%'.$keyw.'%")';
		$order = 'ct_inserttime desc';
		
		$pager->linker = $R->getPageUrl('search/'.$keyw.'/{p}');
		$A->site['site_title'] = $keyw.' _ 产品搜索 _  '.$A->site['site_title'];
		$A->site['site_description'] = '专业生产'.$keyw.'，提供'.$keyw.'价格信息。'.$A->site['site_description'];
		$A->site['site_keywords'] = $keyw.','.$keyw.'价格,'.$A->site['site_keywords'];
		$where .= ' and ct_cid in ('.$p_ids.')';
		$T->bind('banner', 'page_product.jpg');
		$T->bind('categories', $C->getCategories($p_pid));
		
		$items = $pager->parse('ct_id', $field, 'T[content]', $where, $order);
		$T->bind('turnner', $pager->turnner);
		$T->bind('items', $D->fetchAll($items));
		$T->display('product.search');
		break;
	
	case 'page':
		$R->cacheopen = TRUE;
		
		$id = intval($R->getParam(1));
		$page = $C->getContent('*', 'ct_id = '.$id);
		if (empty($page))
		{
			$R->print404();
		}
		$A->site['site_title'] = $page['ct_title'].' _ '.$A->site['site_title'];
		$A->site['site_description'] = $A->strLeft($A->trim($page['ct_content'], TRUE), 220, '...');
		$T->bind('page', $page);
		$T->bind('banner', 'page_'.$id.'.jpg');
		$T->display('page');
		break;
	
	default: return TRUE;
}

return FALSE;
?>