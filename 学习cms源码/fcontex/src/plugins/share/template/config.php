<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>通用分享 - 插件配置 - <?php echo SYSTEM_NAME.' '.SYSTEM_VERSION; ?></title>
<meta name="robots" content="nofollow">
<?php if (0) { ?><link rel="stylesheet" type="text/css" href="../../../module/system/skins/default/style.css" /><?php }else{ ?>
<link rel="stylesheet" type="text/css" href="<?php echo URL_SKIN; ?>style.css" /><?php } ?>
<script type="text/javascript" src="<?php echo URL_SCRIPTS; ?>lib.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo URL_SCRIPTS; ?>lib.system.js"></script>
<script type="text/javascript">
function plugin(mode, args)
{
	switch (mode)
	{
		case 'config':
			$$.post('<?php echo $R->getCtrlUrl(); ?>?plugin=share&mode=' + mode, $('#form_detail').serialize());
			break;
		default:
			$$.alert({text : '无效参数 [ '+mode+' ]。'});
			break;
	}
	
	return false;
}

</script>
</head>
<body>
<?php
$plugin = $P->loadPlugin('share');
$config = $A->loadConfig('plugin.share');
?>
<form id="form_detail" name="form_detail" method="post" action="###" onsubmit="return plugin('config');">
<table class="table_form" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<th>插件描述</th>
		<td><span><?php echo $plugin->basic['desc']; ?></span></td>
	</tr>
	<tr>
		<th>使用说明</th>
		<td>
			<span>
			默认使用Jiathis分享服务，可以更换为其它分享服务，如：<br />
			百度分享：<a href="http://share.baidu.com/" target="_blank">http://share.baidu.com/</a><br />
			bShare分享：<a href="http://www.bshare.cn/" target="_blank">http://www.bshare.cn/</a><br />
			访问上述网站获取分享代码，粘贴到下面的“调用代码”框中即可。
			</span>
		</td>
	</tr>
	<tr>
		<th>调用代码</th>
		<td>
        	<textarea name="code" id="code"><?php echo isset($config['code']) ? $config['code'] : ''; ?></textarea><br />
			<span>支持的标签：{summary}摘要 &nbsp; {title}标题，{picture}图片</span>
		</td>
	</tr>
	<tr class="action"><th>&nbsp;</th><td><input type="submit" class="button" value="保存更改" /><input type="button" class="button cancle" value="放弃更改" onclick="$$.confirm({text:'放弃添加并关闭窗口？', ok:function(){top.naviShut('plugin/share/config');}, icon:'WAR'});" /></td></tr>
</table>
</form>
</body>
</html>
