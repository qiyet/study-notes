<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>附件管理</title>
<link rel="stylesheet" type="text/css" href="<?php echo URL_SKIN ?>style.css" />
<script type="text/javascript" src="<?php echo URL_SCRIPTS; ?>lib.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo URL_SCRIPTS; ?>lib.system.js"></script>
<script type="text/javascript" src="<?php echo URL_TOOLS; ?>datepicker/WdatePicker.js"></script>
<script type="text/javascript">
function attacheds(mode, args)
{
	switch (mode)
	{
		case 'fc.file.update':
			var input = $('#at_filename_'+args);
			if (input.val() != input.attr('value2'))
			{
				$$.post('<?php echo $R->getCtrlUrl(); ?>?mode='+mode+'&args='+args, 'at_filename='+input.val(), function()
				{
					input.attr('value2', input.val());
				});
			}
			input.removeClass('c');
			break;
		case 'fc.file.delete':
			args = args || $$.selectval('#table_list :checked:enabled[name=select]');
			if (args == '')
			{
				$$.alert({text:'请选择记录。'});
				return false;
			}
			$$.confirm({text:'确定删除[ #'+args+' ]？', ok:function()
			{
				$$.get('<?php echo $R->getCtrlUrl(); ?>?mode='+mode+'&args=' + args, function(){$$.redirect();});
			}});
			break;
		default:
			$$.alert({text:'请选择操作。'});
	}
	
	return false;
}

$(function()
{
	$('.table_list tr').hover(function()
	{
		$(this).find('.operate').show();	
	},
	function()
	{
		$(this).find('.operate').hide();
	});
});
</script>
</head>
<body>

<?php
$type	   = $A->strGet('type');
$starttime = $A->strGet('starttime');
$stoptime  = $A->strGet('stoptime');
$where = '1=1';
if ($type) $where .= ' and at_isimage = 1';
if ($starttime) $where .= ' and at_time >= '.strtotime($starttime);
if ($stoptime) $where .= ' and at_time <= '.(strtotime($stoptime) + 60*60*24);
$pager = FCApplication::sharedPageTurnner();
$res = $pager->parse('at_id', '*', 'T[attached]', $where, 'at_id desc');
?>
<form method="get" action="###" onsubmit="$$.redirect('<?php echo $R->getPageUrl(); ?>&'+$(this).serialize());return false;">
<table class="table_tools" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
		<th>
        	<input type="hidden" name="mode" value="file" />
            <select id="bnt"><option value="">批量操作</option>
            	<option value="fc.file.delete">删除</option>
            </select>
            <input type="button" value="应用" class="button" onclick="attacheds($(this).parent().find('select').val());" />
			<select name="type">
            	<option value="">全部类型</option>
                <option value="1"<?php if ($type)echo ' selected="selected"'; ?>>图像</option>
            </select>
            <input name="starttime" type="text" class="text date" style="width:80px;" onClick="WdatePicker()" value="<?php echo $starttime; ?>" />-<input name="stoptime" type="text" class="text date" style="width:80px;" onClick="WdatePicker()" value="<?php echo $stoptime; ?>" />
            <input type="submit" value="筛选" class="button" />
			<input type="button" value="重置" class="button" onclick="$$.redirect('<?php echo $R->getPageUrl(); ?>');" />
		</th>
		<td align="right" class="simple"><?php echo $pager->turnner; ?></td>
	</tr>
</table>
</form>
<table id="table_list" class="table_list" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr id="list_caption">
		<th width="3%"><input type="checkbox" class="checkbox" id="selectall" onchange="$('#table_list :checkbox:enabled[name=select]').prop('checked', !$(this).prop('checked')).click();" /></th>
		<th width="5%">#</th>
		<th width="14%">&nbsp;</th>
		<th width="26%" style="text-align:left;">文件</th>
		<th width="20%">下载</th>
		<th width="16%">大小</th>
		<th width="16%">日期</th>
	</tr>
	<?php
	while ($rst = $D->fetch($res))
	{
	?>
	<tr>
		<td><input type="checkbox" class="checkbox" name="select" id="select_<?php echo $rst['at_id']; ?>" value="<?php echo $rst['at_id']; ?>" onchange="var _this = $(this); _this.prop('checked') ? _this.parent().parent().addClass('S') : _this.parent().parent().removeClass('S');" /></td>
		<td><?php echo $rst['at_id']; ?></td>
		<td><?php if ($rst['at_isimage']){ ?><img src="<?php echo $A->getThumb($rst['at_dir'].$rst['at_filenewname'], 200, 200); ?>" width="60" /><?php } else { ?><img src="<?php echo $A->fileIcon($rst['at_suffix']); ?>" /><?php } ?></td>
		<td class="status files_name">
			<?php
			if ($rst['at_remote'])
			{
			?>
			<a href="<?php echo $rst['at_url']; ?>" target="_blank"><b><?php echo $rst['at_title']; ?></b></a><br /><span class="status_y">远程文件</span><br />
			<div class="operate"><a href="<?php echo $rst['at_url']; ?>" target="_blank">查看</a><span>|</span><a href="#" class="delete" onclick="attacheds('fc.file.delete', <?php echo $rst['at_id']; ?>); return false;">删除</a></div>
			<?php
			}
			else
			{
			?>
			<input type="text" class="text" value="<?php echo $rst['at_filename']; ?>" value2="<?php echo $rst['at_filename']; ?>" id="at_filename_<?php echo $rst['at_id']; ?>" onfocus="$(this).addClass('c');" onblur="attacheds('fc.file.update', <?php echo $rst['at_id']; ?>);" /><br /><?php echo strtoupper($rst['at_suffix']); ?><br />
			<div class="operate"><a href="<?php echo $A->getThumb($rst['at_dir'].$rst['at_filenewname']); ?>" target="_blank">查看</a><span>|</span><a href="#" class="delete" onclick="attacheds('fc.file.delete', <?php echo $rst['at_id']; ?>); return false;">删除</a></div>
			<?php
			}
			?>
		</td>
		<td><?php echo $rst['at_hits']; ?></td>
		<td><?php echo $A->transSize($rst['at_size']); ?></td>
		<td><?php echo $A->transDate($rst['at_time']); ?></td>
	</tr>
	<?php
	}
	if ($D->count($res) < 1)
	{
	?>
    <tr>
		<td colspan="7" align="center">暂无记录。</td>
	</tr>
    <?php
	}
	?>
    <tr id="list_caption">
		<th width="3%"><input type="checkbox" class="checkbox" id="selectall" onchange="$('#table_list :checkbox:enabled[name=select]').prop('checked', !$(this).prop('checked')).click();" /></th>
		<th width="5%">#</th>
		<th width="14%">&nbsp;</th>
		<th width="26%" style="text-align:left;">文件</th>
		<th width="20%">用户</th>
		<th width="16%">大小</th>
		<th width="16%">日期</th>
	</tr>
</table>

<table class="table_tools" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
		<th>
            <select><option value="">批量操作</option>
            	<option value="fc.file.delete">删除</option>
            </select> <input type="button" value="应用" class="button" onclick="attacheds($(this).parent().find('select').val());" />
		</th>
		<td align="right"><?php echo $pager->turnner; ?></td>
	</tr>
</table>

</body>
</html>
