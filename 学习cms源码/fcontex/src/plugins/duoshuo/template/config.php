<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>多说评论 - 插件配置 - <?php echo SYSTEM_NAME.' '.SYSTEM_VERSION; ?></title>
<meta name="robots" content="nofollow">
<?php if (0) { ?><link rel="stylesheet" type="text/css" href="../../../module/system/skins/default/style.css" /><?php }else{ ?>
<link rel="stylesheet" type="text/css" href="<?php echo URL_SKIN; ?>style.css" /><?php } ?>
<script type="text/javascript" src="<?php echo URL_SCRIPTS; ?>lib.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo URL_SCRIPTS; ?>lib.system.js"></script>
<script type="text/javascript">
function plugin(mode, args)
{
	switch (mode)
	{
		case 'config':
			$$.post('<?php echo $R->getCtrlUrl(); ?>?plugin=duoshuo&mode=' + mode, $('#form_detail').serialize());
			break;
		default:
			$$.alert({text : '无效参数 [ '+mode+' ]。'});
			break;
	}
	
	return false;
}

</script>
</head>
<body>
<?php
$plugin = $P->loadPlugin('duoshuo');
$config = $A->loadConfig('plugin.duoshuo');
?>
<form id="form_detail" name="form_detail" method="post" action="###" onsubmit="return plugin('config');">
<table class="table_form" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<th>插件描述</th>
		<td><span><?php echo $plugin->basic['desc']; ?></span></td>
	</tr>
	<tr>
		<th>使用说明</th>
		<td>
			<span>
			1、访问多说官方网站<a href="http://duoshuo.com/" target="_blank">http://duoshuo.com/</a>，点击“我要安装”，按提示登录后创建一个站点。<br />
			2、进入多说的“后台管理”界面，点击“设置”后将看到的“密钥”和“名称”填写在下面。<br />
			3、默认情况下，下面的“API地址”和“调用代码”无须修改。
			</span>
		</td>
	</tr>
	<tr>
		<th>API地址</th>
		<td><input class="text" type="text" name="api" id="api" value="<?php echo isset($config['api']) ? $config['api'] : ''; ?>" /></td>
	</tr>
	<tr>
		<th>用户名</th>
		<td><input class="text" type="text" name="short_name" id="short_name" value="<?php echo isset($config['short_name']) ? $config['short_name'] : ''; ?>" /></td>
	</tr>
	<tr>
		<th>密钥</th>
		<td><input class="text" type="text" name="secret" id="secret" value="<?php echo isset($config['secret']) ? $config['secret'] : ''; ?>" /></td>
	</tr>
	<tr>
		<th>调用代码</th>
		<td>
        	<textarea name="code" id="code"><?php echo isset($config['code']) ? $config['code'] : ''; ?></textarea><br />
			<span>支持的标签：{id}内容ID &nbsp; {title}内容标题</span>
		</td>
	</tr>
	<tr>
		<th>在文章正文</th>
		<td>
        	<input type="checkbox" class="checkbox" value="1" name="hook_read" id="hook_read"<?php if(isset($config['hook_read']) && $config['hook_read'])echo ' checked="checked"'; ?> /> <label for="hook_read">启用多说评论</label>
		</td>
	</tr>
    <tr>
		<th>在普通页面</th>
		<td>
        	<input type="checkbox" class="checkbox" value="1" name="hook_page" id="hook_page"<?php if(isset($config['hook_page']) && $config['hook_page'])echo ' checked="checked"'; ?> /> <label for="hook_page">启用多说评论</label>
		</td>
	</tr>
    <tr>
		<th>在留言页面</th>
		<td>
        	<input type="checkbox" class="checkbox" value="1" name="hook_gbook" id="hook_gbook"<?php if(isset($config['hook_gbook']) && $config['hook_gbook'])echo ' checked="checked"'; ?> /> <label for="hook_gbook">启用多说评论</label>
		</td>
	</tr>
	<tr class="action"><th>&nbsp;</th><td><input type="submit" class="button" value="保存更改" /><input type="button" class="button cancle" value="放弃更改" onclick="$$.confirm({text:'放弃添加并关闭窗口？', ok:function(){top.naviShut('plugin/duoshuo/config');}, icon:'WAR'});" /></td></tr>
</table>
</form>
</body>
</html>
