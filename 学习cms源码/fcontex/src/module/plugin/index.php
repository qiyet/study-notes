<?php
/***
 * 名称：插件模块调度程序
 * Alan 2013.05
 * www.fcontex.com
*/

if (!defined('SYSTEM_INCLUDE')) exit('Access Denied.');

switch ($R->action)
{
	////前端请求////
	
	////后端请求////
	case 'fc.plugin.select':
		$U->checkRights('plugin.select');
		$T->show('plugin.select');
		break;
	
	default:
		$R->action = $R->getParam(2, 0);
		$path = PATH_PLUGINS.$R->plugin.'/index.php';
		if (!file_exists($path))
		{
			//$R->cacheopen = TRUE;
			$R->header404();
			$T->display('404');
		}
		else include PATH_PLUGINS.$R->plugin.'/index.php';
		break;
}
?>