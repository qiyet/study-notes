<?php
/***
 * 名称：插件内部动作处理程序
 * Alan 2013.05
 * www.fcontex.com
*/

switch ($mode = $A->strGet('mode'))
{
	case 'config':
		$U->checkRights('plugin.update', TRUE);
		
		$config = $A->loadConfig('plugin.duoshuo');
		
		$config['api'] 			= $A->strPost('api', FALSE);
		$config['short_name'] 	= $A->strPost('short_name', FALSE);
		$config['secret'] 		= $A->strPost('secret', FALSE);
		$config['code'] 		= $A->strPost('code', FALSE);
		$config['hook_read']	= intval($A->strPost('hook_read')) ? TRUE : FALSE;
		$config['hook_page'] 	= intval($A->strPost('hook_page')) ? TRUE : FALSE;
		$config['hook_gbook'] 	= intval($A->strPost('hook_gbook')) ? TRUE : FALSE;
		
		$A->saveConfig('plugin.duoshuo', $config);
		$P->hooksUpdate('duoshuo');
		$A->logInsert('更换了插件配置 # duoshuo');
		echo 'YES';
		break;
	
	case 'sync':
		$config = FCApplication::loadConfig('plugin.duoshuo');
		$input = $_POST;
		$action = $input['action'];
		$signature = $input['signature'];
		unset($input['signature']);
		if ($action != 'sync_log')
		{
			$A->logInsert('多说同步action错误。');
			exit('Bad action.');
		}
		ksort($input);
		$baseString = http_build_query($input, null, '&');
		$expectSignature = base64_encode(hash_hmac('sha1', $baseString, $config['secret'], true));
		if ($signature != $expectSignature)
		{
			$A->logInsert('多说同步signature错误。');
			exit('Bad signature.');
		}
		$url  = $config['api'];
		$url .= '?short_name='.$config['short_name'];
		$url .= '&secret='.$config['secret'];
		$url .= '&since_id='.$config['since_id'];
		$json = json_decode(file_get_contents($url));
		if (!$json || $json->code != 0)
		{
			$A->logInsert('多说同步response错误。');
			exit('Bad response.');
		}
		$count = 0;
		//$iparea = $A->loadLibrary('iparea');
		foreach ($json->response as $res)
		{
			$config['since_id'] = $res->log_id;
			if ($res->action != 'create')
			{
				continue;
			}
			else $count++;
			$post = $res->meta;
			if ($post->thread_key == 0)
			{
				$parent = $D->fetch($D->query('select gb_id, gb_topid, gb_name from T[gbook] where gb_postid = "'.$post->parent_id.'"'));
				if ($parent)
				{
					$post->parent_id = $parent['gb_id'];
					$topid = $parent['gb_topid'] ? $parent['gb_topid'] : $parent['gb_id'];
					$toname = $parent['gb_name'];
				}
				else
				{
					$post->parent_id = 0;
					$topid = 0;
					$toname = '';
				}
				$array = array
				(
					'gb_toid'	=> $post->parent_id,
					'gb_topid'	=> $topid,
					'gb_name'	=> $post->author_name,
					'gb_toname'	=> $toname,
					'gb_email'	=> $post->author_email,
					'gb_url'	=> str_ireplace('http://', '', $post->author_url),
					'gb_content'=> $post->message,
					'gb_ip'		=> $post->ip,
					//'gb_iparea'	=> $iparea->dataMini($post->ip),
					'gb_check'	=> 1,
					'gb_time'	=> strtotime($post->created_at),
					'gb_update' => strtotime($post->created_at),
					'gb_postid' => $post->post_id
				);
				$D->insert('T[gbook]', $array);
				if ($post->parent_id) $D->update('T[gbook]', array('gb_update' => time()), array('gb_id' => $post->parent_id));
			}
			else
			{
				$thread = $C->getContent('ct_title', 'ct_id='.$post->thread_key);
				$parent = $D->fetch($D->query('select cm_id, cm_topid, cm_name from T[comment] where cm_postid = "'.$post->parent_id.'"'));
				if ($parent)
				{
					$post->parent_id = $parent['cm_id'];
					$topid = $parent['cm_topid'] ? $parent['cm_topid'] : $parent['cm_id'];
					$toname = $parent['cm_name'];
				}
				else
				{
					$post->parent_id = 0;
				}
				if (!$thread)
				{
					$thread = array('ct_title'=>'');
				}
				$array = array
				(
					'cm_cid'	=> $post->thread_key,
					'cm_ctitle'	=> $thread['ct_title'],
					'cm_toid'	=> $post->parent_id,
					'cm_topid'	=> $topid,
					'cm_name'	=> $post->author_name,
					'cm_toname'	=> $toname,
					'cm_email'	=> $post->author_email,
					'cm_url'	=> str_ireplace('http://', '', $post->author_url),
					'cm_content'=> $post->message,
					'cm_ip'		=> $post->ip,
					//'cm_iparea'	=> $iparea->dataMini($post->ip),
					'cm_check'	=> 1,
					'cm_time'	=> strtotime($post->created_at),
					'cm_update' => strtotime($post->created_at),
					'cm_postid' => $post->post_id
				);
				$D->insert('T[comment]', $array);
				$D->query('update T[content] set ct_talks=ct_talks+1 where ct_id='.$post->thread_key);
				if ($post->parent_id) $D->update('T[comment]', array('cm_update' => time()), array('cm_id' => $post->parent_id));
			}
		}
		FCApplication::saveConfig('plugin.duoshuo', $config);
		$A->logInsert('多说同步：新增了'.$count.'条记录。');
		echo 'YES|数据同步完成。';
		break;
		
	default:
		echo 'ERR|无效请求。';
		break;
}
?>