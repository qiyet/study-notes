/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : fcontex1

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2014-07-27 22:51:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `fc_attached`
-- ----------------------------
DROP TABLE IF EXISTS `fc_attached`;
CREATE TABLE `fc_attached` (
  `at_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `at_uid` int(10) unsigned DEFAULT '0',
  `at_hits` int(10) unsigned DEFAULT '0',
  `at_filename` varchar(100) DEFAULT NULL,
  `at_filenewname` varchar(100) DEFAULT NULL,
  `at_title` varchar(100) DEFAULT NULL,
  `at_isimage` tinyint(1) unsigned DEFAULT '0',
  `at_size` int(10) unsigned DEFAULT '0',
  `at_suffix` varchar(10) DEFAULT NULL,
  `at_filetype` varchar(100) DEFAULT NULL,
  `at_dir` varchar(100) DEFAULT NULL,
  `at_remote` int(10) DEFAULT '0',
  `at_url` varchar(200) DEFAULT NULL,
  `at_time` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`at_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fc_attached
-- ----------------------------
INSERT INTO `fc_attached` VALUES ('1', '1', '0', 'png-1086.png', '735951354083439.png', '', '1', '9791', 'png', 'application/octet-stream', 'member/201211/28/', '0', '', '1354083439');
INSERT INTO `fc_attached` VALUES ('14', '1', '0', 'news1.png', '126241406465411.png', null, '1', '270841', 'png', 'application/octet-stream', 'content/201407/27/', '0', null, '1406465411');
INSERT INTO `fc_attached` VALUES ('4', '1', '0', 'focus1.jpg', '674641406440125.jpg', null, '1', '51813', 'jpg', 'application/octet-stream', 'content/201407/27/', '0', null, '1406440125');
INSERT INTO `fc_attached` VALUES ('9', '1', '0', 'focus2.jpg', '281111406455976.jpg', null, '1', '50665', 'jpg', 'application/octet-stream', 'content/201407/27/', '0', null, '1406455976');
INSERT INTO `fc_attached` VALUES ('10', '1', '0', 'product1.jpg', '264561406456533.jpg', null, '1', '27660', 'jpg', 'application/octet-stream', 'content/201407/27/', '0', null, '1406456533');
INSERT INTO `fc_attached` VALUES ('11', '1', '0', 'product2.jpg', '960971406456819.jpg', null, '1', '20008', 'jpg', 'application/octet-stream', 'content/201407/27/', '0', null, '1406456819');
INSERT INTO `fc_attached` VALUES ('12', '1', '0', 'product3.jpg', '456871406456841.jpg', null, '1', '31363', 'jpg', 'application/octet-stream', 'content/201407/27/', '0', null, '1406456841');
INSERT INTO `fc_attached` VALUES ('13', '1', '0', 'product4.jpg', '124561406456864.jpg', null, '1', '12974', 'jpg', 'application/octet-stream', 'content/201407/27/', '0', null, '1406456864');

-- ----------------------------
-- Table structure for `fc_category`
-- ----------------------------
DROP TABLE IF EXISTS `fc_category`;
CREATE TABLE `fc_category` (
  `cg_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cg_pid` int(10) unsigned DEFAULT '0',
  `cg_order` int(10) unsigned DEFAULT '0',
  `cg_type` tinyint(1) unsigned DEFAULT '0',
  `cg_url` varchar(200) DEFAULT NULL,
  `cg_target` varchar(10) DEFAULT '_self',
  `cg_title` varchar(100) DEFAULT NULL,
  `cg_desc` varchar(200) DEFAULT NULL,
  `cg_show` int(10) DEFAULT '0',
  `cg_count` int(10) DEFAULT '0',
  `cg_time` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`cg_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fc_category
-- ----------------------------
INSERT INTO `fc_category` VALUES ('1', '0', '0', '0', '', '_self', '独立页面', '', '1', '2', '1406433540');
INSERT INTO `fc_category` VALUES ('2', '0', '0', '0', '', '_self', '产品', '', '1', '0', '1406433582');
INSERT INTO `fc_category` VALUES ('3', '0', '0', '0', '', '_self', '新闻', '', '1', '0', '1406433601');
INSERT INTO `fc_category` VALUES ('4', '0', '0', '0', '', '_self', '首页滚动大图', '', '1', '2', '1406433620');
INSERT INTO `fc_category` VALUES ('5', '2', '0', '0', '', '_self', '主营产品', '', '1', '4', '1406437264');
INSERT INTO `fc_category` VALUES ('6', '2', '0', '0', '', '_self', '产品配件', '', '1', '0', '1406437281');
INSERT INTO `fc_category` VALUES ('7', '3', '0', '0', '', '_self', '公司新闻', '', '1', '1', '1406437312');
INSERT INTO `fc_category` VALUES ('8', '3', '0', '0', '', '_self', '行业新闻', '', '1', '1', '1406437321');

-- ----------------------------
-- Table structure for `fc_comment`
-- ----------------------------
DROP TABLE IF EXISTS `fc_comment`;
CREATE TABLE `fc_comment` (
  `cm_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cm_cid` int(10) unsigned DEFAULT '0',
  `cm_ctitle` varchar(200) DEFAULT NULL,
  `cm_toid` int(10) unsigned DEFAULT '0',
  `cm_topid` int(10) unsigned DEFAULT '0',
  `cm_uid` int(10) unsigned DEFAULT '0',
  `cm_name` varchar(100) DEFAULT NULL,
  `cm_toname` varchar(100) DEFAULT NULL,
  `cm_email` varchar(100) DEFAULT NULL,
  `cm_url` varchar(200) DEFAULT NULL,
  `cm_content` varchar(10000) DEFAULT NULL,
  `cm_ip` varchar(100) DEFAULT NULL,
  `cm_iparea` varchar(100) DEFAULT NULL,
  `cm_check` tinyint(1) unsigned DEFAULT '0',
  `cm_time` int(10) unsigned DEFAULT '0',
  `cm_update` int(10) unsigned DEFAULT '0',
  `cm_postid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`cm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fc_comment
-- ----------------------------

-- ----------------------------
-- Table structure for `fc_content`
-- ----------------------------
DROP TABLE IF EXISTS `fc_content`;
CREATE TABLE `fc_content` (
  `ct_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ct_type` tinyint(1) unsigned DEFAULT '0',
  `ct_cid` int(10) unsigned DEFAULT '0',
  `ct_ctitle` varchar(200) DEFAULT NULL,
  `ct_uid` int(10) unsigned DEFAULT '0',
  `ct_username` varchar(200) DEFAULT NULL,
  `ct_title` varchar(200) DEFAULT NULL,
  `ct_title2` varchar(200) DEFAULT NULL,
  `ct_hits` int(10) unsigned DEFAULT '0',
  `ct_talks` int(10) unsigned DEFAULT '0',
  `ct_tags` varchar(200) DEFAULT NULL,
  `ct_cover` varchar(200) DEFAULT NULL,
  `ct_summary` varchar(10000) DEFAULT NULL,
  `ct_content` text,
  `ct_check` tinyint(1) unsigned DEFAULT '0',
  `ct_fixed` tinyint(1) unsigned DEFAULT '0',
  `ct_quiet` tinyint(1) unsigned DEFAULT '0',
  `ct_inserttime` int(10) unsigned DEFAULT '0',
  `ct_updatetime` int(10) unsigned DEFAULT '0',
  `ct_seo` tinyint(1) unsigned DEFAULT '0',
  `ct_pagetitle` varchar(200) DEFAULT NULL,
  `ct_keywords` varchar(200) DEFAULT NULL,
  `ct_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ct_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fc_content
-- ----------------------------
INSERT INTO `fc_content` VALUES ('1', '1', '1', '关于', '1', 'admin', '关于', '', '50', '0', '', '', '', '<strong>fcontex</strong>是开源的PHP博客系统，拥有漂亮的Web 2.0风格的后台界面和完全Ajax化的流畅操作体验，可用于快速构建中小型企业网站和个人博客（Blog）。<br />\r\nfcontex是专为二次开发而设计的，主要体现在以下两点：<br />\r\n1、基于完全模块化方式设计和开发，且仿照内置模块能快速上手fcontex模块开发；<br />\r\n2、基于dispatcher.php主题定制文件，可以实现任意风格的定制主题而不需要改动fcontex的原有代码。<br />\r\n<br />\r\n<strong>fcontex</strong>是free和context两个单词的连接简化，寓意自由、流畅的上下文操作体验。<br />', '1', '0', '0', '1406435136', '1406436311', '0', '', '', '');
INSERT INTO `fc_content` VALUES ('2', '1', '1', '关于', '1', 'admin', '联系', '', '30', '0', '', '', '', '邮箱：alan@fcontex.com<br />\r\n网站：www.fcontex.com<br />', '1', '0', '0', '1406435352', '1406435460', '0', '', '', '');
INSERT INTO `fc_content` VALUES ('3', '1', '4', '首页滚动大图', '1', 'admin', '滚动大图1', '/?product/8.html', '0', '0', '', 'content/201407/27/674641406440125.jpg', '', '', '1', '0', '0', '1406439682', '1406466262', '0', '', '', '');
INSERT INTO `fc_content` VALUES ('4', '1', '4', '首页滚动大图', '1', 'admin', '滚动图片2', '/?product/7.html', '0', '0', '', 'content/201407/27/281111406455976.jpg', '', '', '1', '0', '0', '1406436622', '1406466294', '0', '', '', '');
INSERT INTO `fc_content` VALUES ('5', '1', '5', '主营产品', '1', 'admin', '产品1', '', '2', '0', '手机,安卓,互联网', 'content/201407/27/264561406456533.jpg', '产品1简介', '<img atl=\"product1.jpg\" src=\"/store/upload/content/201407/27/264561406456533.jpg\" />', '1', '1', '0', '1406456400', '1406466033', '0', '', '', '');
INSERT INTO `fc_content` VALUES ('6', '1', '5', '主营产品', '1', 'admin', '产品2', '', '0', '0', '手机,互联网,安卓', 'content/201407/27/960971406456819.jpg', '产品2简介', '<img atl=\"product2.jpg\" src=\"/store/upload/content/201407/27/960971406456819.jpg\" />', '1', '1', '0', '1406456806', '1406466039', '0', '', '', '');
INSERT INTO `fc_content` VALUES ('7', '1', '5', '主营产品', '1', 'admin', '产品3', '', '1', '0', '平板,互联网,安卓', 'content/201407/27/456871406456841.jpg', '产品3简介', '<img atl=\"product3.jpg\" src=\"/store/upload/content/201407/27/456871406456841.jpg\" />', '1', '1', '0', '1406456825', '1406466047', '0', '', '', '');
INSERT INTO `fc_content` VALUES ('8', '1', '5', '主营产品', '1', 'admin', '产品4', '', '7', '0', '手机,互联网,安卓', 'content/201407/27/124561406456864.jpg', '产品4简介', '<img atl=\"product4.jpg\" src=\"/store/upload/content/201407/27/124561406456864.jpg\" /><br />', '1', '1', '0', '1406456847', '1406466055', '0', '', '', '');
INSERT INTO `fc_content` VALUES ('9', '1', '8', '行业新闻', '1', 'admin', '示例新闻1', '', '2', '0', '手机,互联网', 'content/201407/27/126241406465411.png', '新闻1内容概要', '', '1', '0', '0', '1406464914', '1406465474', '0', '', '', '');

-- ----------------------------
-- Table structure for `fc_gbook`
-- ----------------------------
DROP TABLE IF EXISTS `fc_gbook`;
CREATE TABLE `fc_gbook` (
  `gb_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gb_toid` int(10) unsigned DEFAULT '0',
  `gb_topid` int(10) unsigned DEFAULT '0',
  `gb_uid` int(10) unsigned DEFAULT '0',
  `gb_name` varchar(100) DEFAULT NULL,
  `gb_toname` varchar(100) DEFAULT NULL,
  `gb_email` varchar(100) DEFAULT NULL,
  `gb_url` varchar(200) DEFAULT NULL,
  `gb_content` varchar(10000) DEFAULT NULL,
  `gb_ip` varchar(100) DEFAULT NULL,
  `gb_iparea` varchar(100) DEFAULT NULL,
  `gb_check` tinyint(1) unsigned DEFAULT '0',
  `gb_time` int(10) unsigned DEFAULT '0',
  `gb_update` int(10) unsigned DEFAULT '0',
  `gb_postid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`gb_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fc_gbook
-- ----------------------------

-- ----------------------------
-- Table structure for `fc_group`
-- ----------------------------
DROP TABLE IF EXISTS `fc_group`;
CREATE TABLE `fc_group` (
  `gr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gr_name` varchar(100) DEFAULT NULL,
  `gr_desc` varchar(200) DEFAULT NULL,
  `gr_rights` varchar(10000) DEFAULT NULL,
  `gr_time` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`gr_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fc_group
-- ----------------------------
INSERT INTO `fc_group` VALUES ('1', '系统管理员', '系统管理员权限不受限制。', 'system.login,system.site.select,system.site.update,system.modules.select,system.modules.insert,system.modules.update,system.modules.disable,system.modules.order,system.modules.delete,system.logs.select,system.logs.delete,system.file.select,system.file.update,system.file.delete,system.file.upload,system.cache,system.skins,system.themes,user.group.select,user.group.insert,user.group.update,user.group.delete,user.select,user.insert,user.update,user.delete,content.category.select,content.category.insert,content.category.update,content.category.order,content.category.delete,content.select,content.insert,content.update,content.delete,content.page.select,content.page.insert,content.page.update,content.page.delete,content.tags.select,content.tags.update,content.tags.delete,content.navigate.select,content.navigate.insert,content.navigate.update,content.navigate.delete,plugin.select,plugin.insert,plugin.update,plugin.disable,plugin.order,plugin.delete,gbook.select,gbook.update,gbook.delete,comment.select,comment.update,comment.delete', '1340031851');
INSERT INTO `fc_group` VALUES ('2', '普通管理员', '普通管理员权限受限制。', 'system.login,user.group.select,user.select,content.category.select,content.insert,content.update,content.delete,content.tags.select,content.tags.update,content.tags.delete,gbook.select,gbook.update,gbook.delete,comment.select,comment.update,comment.delete', '1340464421');

-- ----------------------------
-- Table structure for `fc_logs`
-- ----------------------------
DROP TABLE IF EXISTS `fc_logs`;
CREATE TABLE `fc_logs` (
  `lg_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lg_event` varchar(100) DEFAULT NULL,
  `lg_uid` int(10) unsigned DEFAULT '0',
  `lg_ip` varchar(100) DEFAULT NULL,
  `lg_iparea` varchar(100) DEFAULT NULL,
  `lg_time` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`lg_id`)
) ENGINE=MyISAM AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fc_logs
-- ----------------------------
INSERT INTO `fc_logs` VALUES ('131', '清理了日志', '1', '', null, '1406472610');

-- ----------------------------
-- Table structure for `fc_navigate`
-- ----------------------------
DROP TABLE IF EXISTS `fc_navigate`;
CREATE TABLE `fc_navigate` (
  `nv_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nv_title` varchar(100) DEFAULT NULL,
  `nv_url` varchar(200) DEFAULT NULL,
  `nv_target` varchar(10) DEFAULT NULL,
  `nv_order` int(10) unsigned DEFAULT '0',
  `nv_check` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`nv_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fc_navigate
-- ----------------------------
INSERT INTO `fc_navigate` VALUES ('1', '首页', '/', '0', '1', '1');
INSERT INTO `fc_navigate` VALUES ('2', '产品', '/?product.html', '0', '2', '1');
INSERT INTO `fc_navigate` VALUES ('3', '新闻', '/?news.html', '0', '3', '1');
INSERT INTO `fc_navigate` VALUES ('4', '关于', '/?page/1.html', '0', '4', '1');
INSERT INTO `fc_navigate` VALUES ('5', '联系', '/?page/2.html', '0', '5', '1');

-- ----------------------------
-- Table structure for `fc_tags`
-- ----------------------------
DROP TABLE IF EXISTS `fc_tags`;
CREATE TABLE `fc_tags` (
  `tg_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tg_title` varchar(100) DEFAULT NULL,
  `tg_color` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`tg_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fc_tags
-- ----------------------------
INSERT INTO `fc_tags` VALUES ('4', '平板', null);
INSERT INTO `fc_tags` VALUES ('3', '手机', null);
INSERT INTO `fc_tags` VALUES ('5', '互联网', null);
INSERT INTO `fc_tags` VALUES ('6', '安卓', null);

-- ----------------------------
-- Table structure for `fc_user`
-- ----------------------------
DROP TABLE IF EXISTS `fc_user`;
CREATE TABLE `fc_user` (
  `us_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `us_group` int(10) unsigned DEFAULT '0',
  `us_username` varchar(100) DEFAULT NULL,
  `us_password` varchar(100) DEFAULT NULL,
  `us_name` varchar(100) DEFAULT NULL,
  `us_desc` varchar(200) DEFAULT NULL,
  `us_email` varchar(100) DEFAULT NULL,
  `us_phone` varchar(100) DEFAULT NULL,
  `us_skin` varchar(100) DEFAULT NULL,
  `us_face` varchar(100) DEFAULT NULL,
  `us_time` int(10) unsigned DEFAULT '0',
  `us_resetcodetime` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`us_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fc_user
-- ----------------------------
INSERT INTO `fc_user` VALUES ('1', '1', 'admin', 'Dkocttvx', 'admin', '', '', '', 'default', 'member/201211/28/735951354083439.png', '1341129209', '0');
