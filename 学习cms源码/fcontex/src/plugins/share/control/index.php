<?php
/***
 * 名称：插件内部动作处理程序
 * Alan 2013.05
 * www.fcontex.com
*/

switch ($mode = $A->strGet('mode'))
{
	case 'config':
		$U->checkRights('plugin.update', TRUE);
		
		$config = $A->loadConfig('plugin.share');
		$config['code'] = $A->strPost('code', FALSE);
		$A->saveConfig('plugin.share', $config);
		$P->hooksUpdate('share');
		$A->logInsert('更换了插件配置 # share');
		echo 'YES';
		break;
	
	default:
		echo 'ERR|无效请求。';
		break;
}
?>