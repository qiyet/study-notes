<?php
/***
 * 名称：系统模块调度程序
 * Alan 2012.03
 * www.fcontex.com
*/

if (!defined('SYSTEM_INCLUDE')) exit('Access Denied.');

switch ($R->action)
{
	////前端请求////
	case 'file':
		$id = intval($A->strDeCode($R->getParam(2)));
		
		$rst = $D->fetch($D->query('select at_filename, at_filenewname, at_dir, at_remote, at_url from T[attached] where at_id='.$id));
		if (empty($rst))
		{
			$R->print404();
		}
		$fname = PATH_STORE.$A->system['uploadDir'].'/'.$rst['at_dir'].$rst['at_filenewname'];
		if (!file_exists($fname))
		{
			$R->print404();
		}
		$fsize = filesize($fname);
		if ($fsize/1024/1024 > $A->system['uploadSize'])
		{
			$R->print404();
		}
		
		if (!isset($_SESSION['file_'.$id]) || time()-intval($_SESSION['file_'.$id])>2)
		{
			$_SESSION['file_'.$id] = time();
			$D->query('update T[attached] set at_hits=at_hits+1 where at_id='.$id);
		}
		
		if ($rst['at_remote'])
		{
			header('Location: '.$rst['at_url']);
		}
		else
		{
			/*////支持断点续传////
			$range = 0;
			if(!empty($_SERVER['HTTP_RANGE']))
			{
				list($range) = explode('-', (str_replace('bytes=','',$_SERVER['HTTP_RANGE'])));
			}
			if (($length = $fsize-intval($range)) <= 0) $length = 0;
			header('HTTP/1.1 206 Partial Content');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.$rst['at_filename']);
			header('Content-Length: '.$length);
			header('Content-Range: bytes='.$range.'-'.($fsize-1).'/'.($fsize));
			$fp = fopen($fname, 'rb');
			fseek($fp, $range);
			echo fread($fp, $fsize);
			fclose($fp);
			flush();ob_flush();
			//////////////////*/
			
			////不支持断点续传////
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.$rst['at_filename']);
			header('Content-Length: '.$fsize);
			$fp = fopen($fname, 'rb');
			echo fread($fp, $fsize);
			fclose($fp);
			flush();ob_flush();
			////////////////////
		}
		
		break;
	
	////后端请求////
	case 'fc.menus':
		$U->checkRights('system.login');
		$T->show('menus');
		break;
	
	case 'fc.site':
		$U->checkRights('system.site.select');
		$T->show('config.site');
		break;
	
	case 'fc.logs':
		$U->checkRights('system.logs.select');
		$T->show('logs');
		break;
	
	case 'fc.cache':
		$U->checkRights('system.cache');
		$T->show('cache');
		break;
	
	case 'fc.modules':
		$U->checkRights('system.modules.select');
		$T->show('modules');
		break;
	
	case 'fc.files.upload':
		$U->checkRights('system.file.upload');
		$T->show('files.upload');
		break;
	
	case 'fc.files.url':
		$U->checkRights('system.file.upload');
		$T->show('files.url');
		break;
	
	case 'fc.files.server':
		$U->checkRights('system.file.upload');
		$T->show('files.server');
		break;
	
	case 'fc.files':
		$U->checkRights('system.file.select');
		$T->show('files.select');
		break;
	
	case 'fc.config.skins':
		$U->checkRights('system.skins');
		$T->show('config.skins');
		break;
	
	case 'fc.config.themes':
		$U->checkRights('system.themes');
		$T->show('config.themes');
		break;
	
	case 'fc.login.status';
		$T->show('login.status');
		break;
	
	default:
		$T->show('start');
		break;
}
?>